## Architecture des systèmes embarqués: Les appels de fonctions et la pile

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction. Applications et problématiques des systèmes embarqués
2. Architecture d’un microcontrôleur, d’un SoC (2 vidéos)
3. **Les appels de fonctions et la pile**
4. Les interruptions

[![Vidéo 3 B4-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S3-V3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S3-V3.mp4)

## Présentation

[Support de présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/bloc_4_architecture_robotique_reseaux_ihm/4-1_architecture_materielle/4-1-3_Architecture_des_syst%C3%A8mes_embarqu%C3%A9s/4-1-3_Texte_SystemesEmbarques.md)
