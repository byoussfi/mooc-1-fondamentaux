# Sécurité 3/5: Les protocoles SSH/SSL

1. Firewall / Proxy / DMZ
2. Chiffrement
3. **Les protocoles SSH/SSL**
4. VPN
5. VLAN

[![Vidéo 3 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video3-SSH_et_SSL.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video3-SSH_et_SSL.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
