# Routage 5/5: Le routage statique

1. Le routage statique
2. Le protocole NAT
3. Le routage dynamique - Le protocole RIP
4. Le routage dynamique - Le protocole OSPF
5. **Le routage dynamique - Le protocole BGP**

- Dans cette vidéo, **Anthony Juton**...

[![Vidéo 3part3 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_video3-3_BGP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_video3-3_BGP.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
