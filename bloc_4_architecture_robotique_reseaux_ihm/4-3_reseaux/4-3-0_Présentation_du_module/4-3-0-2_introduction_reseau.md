# Module 4.3 Réseaux, Ethernet TCP/IP

L'objectif de ce module est de donner les bases des réseaux en général et une maitrise des principaux concepts et outils des réseaux Ethernet/TCP/IP.
Deux projets accompagneront l'apprenant au cours de ce module : 
* Création d'un serveur local Web / SSH / VPN accessible depuis l'extérieur
* Création d'un réseau plus important sur Cisco Packet Tracer (fonction de ce réseau encore à définir)

Des exercices sur FunMooc et sur Gitlab sont aussi l’occasion d’apporter des éléments complémentaire aux cours vidéos qui se limitent aux concepts importants.

## Présentation du module
les formateurs, les objectifs, les 2 architectures (PI+wireshark et pt) fils rouge du module, non obligatoires, l’évaluation. (vidéo) *Tous*
	
## Chapitre 1 - Introduction
* Introduction aux réseaux : histoire, les différents types de réseaux, les caractéristiques des réseaux, vocabulaire (vidéo)
* quizz (quel type de réseau pour quelle appli ?) *Anthony*
* Un rappel sur Analogique/numérique et sur hexadécimal avec exercice (conversion d’un signal analogique en num et conversion décimal/hexa/binaire) (exos sur la plateforme) *Anthony*Voir dans le programme ce qui concerne les réseaux autres que TCP/IP (la liaison série, BT, IoT ?)
* Le modèle OSI / Les couches TCP/IP → Le plan du cours (vidéo) *Josiane*

## Ouverture aux autres réseaux
* Liaison série + quizz 

## Chapitre 2 : Couches physiques Ethernet
* Le rôle de la couche physique, différentes couches physiques, les câbles, les fibres optiques (vidéo) *Anthony*
* quizz avec des calculs de débit, shannon *Tara*
* La trame Ethernet, les adresses MAC, le CRC (un fichier statique) *Josiane*
* quizz avec des extraits de wireshark *Tara*
* Le switch (un mot sur le hub), tables CAM. *Josiane*
* un exercice sur Switch/TableCAM/Reconfiguration… (voir avec Isabelle pour quelque chose de dynamique ou sur cisco pt) *Anthony*
* Le Wifi en mode conférence. (vidéo) *Tara*
* Quizz Wifi QCM *Tara*
	
## Chapitre 3 : Réseau IP
* L’intérêt de l’adresses IP / Masque / ipv4 (vidéo) *Anthony*
* exercices de masques/sous-réseau *Anthony*
* exercice Raspberry Pi et wireshark et ARP/Ping *Anthony*1
* IPv6 – cohabitation avec ipv4 (vidéo) *Tara*
* Exo reconnaissances des ipv6 *Tara*
* DHCP *Tara*
* Quizz avec des échanges DHCP acquis sur wireshark *Tara*

## Chapitre 4 : Routage
* Routage statique local / NAT *Josiane*
* Exercice pratique sur Wireshark traceroute / DHCP requêtes vers un site type localiser ip ou un serveur fait (sur PC) *Josiane*
* Exercice sur Cisco PacketTracer – ping – routeur – NAT (introduire une machine et réparer un réseau troubleshooting) *Josiane* 
* Routage dynamique RIP / OSPF *Anthony*
* Exercice sur Cisco PacketTracer routage dynamique *Anthony*
* Quizz *Anthony*

## Chapitre 5 : Transport TCP/UDP
* TCP/UDP, les ports, les connexions TCP… + quizz
* Exercice sur les ports, les routeurs, le NAT
* Qualité de service et routage sur Internet avec démo RiverBed + quizz (Niveau Taxonomique 2)
* Exercice pratique : installer un serveur Web chez soi avec redirection de ports sur Raspberry Pi ou sur un PC

## Chapitre 6 : Application / Service
* DNS + quizz
* Exercice pratique + *Tara* a une application intéressante sur DNS
* HTTP/FTP/SMTP
* Exercice pratique wireshark avec HTTP et streaming

## Chapitre 7 : Sécurité
* Firewall / Proxy / DMZ
* Exercice Firewall iptables
* Chiffrement – clé sysmétrique / asymétrique SSH/SSL
* Exercice Chiffrement asymétrique
* VPN + quizz
* VLAN + quizz
* TP Cisco Packet Tracer VLAN VPN DMZ
* Exo pratique serveur VPN/SSH sur Raspberry PI
	
	
	
