## Arbres et codage. 4/4 : Un codage optimal

1. Introduction
2. Transmission de l'information
3. Codage
4. **Un codage optimal**

[![Vidéo 4 B3-M1-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S7-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S7-V4.mp4)

## Présentation 

[Support des vidéos de la séquence Arbres et codage](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B3-M1/B3-M1-S7.pdf)
