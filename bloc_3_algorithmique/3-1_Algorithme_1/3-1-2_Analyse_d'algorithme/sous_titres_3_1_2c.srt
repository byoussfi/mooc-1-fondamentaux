1
00:00:00,926 --> 00:00:02,712
Quand on va parler de complexité,

2
00:00:02,812 --> 00:00:05,093
il y a une première chose qu'il faut faire,

3
00:00:05,193 --> 00:00:06,500
c'est de savoir de quoi on parle.

4
00:00:07,223 --> 00:00:08,569
Donc on va choisir

5
00:00:08,669 --> 00:00:10,455
un certain nombre d'opérateurs

6
00:00:10,555 --> 00:00:11,324
dans mon algorithme

7
00:00:11,424 --> 00:00:13,032
que je vais avoir à compter.

8
00:00:13,378 --> 00:00:15,200
Ces opérateurs que j'ai à compter,

9
00:00:15,300 --> 00:00:16,109
ça va me dire

10
00:00:16,209 --> 00:00:18,818
lors de l'exécution de l'algorithme,

11
00:00:18,918 --> 00:00:20,998
vous aurez utilisé,

12
00:00:21,098 --> 00:00:23,166
je ne sais pas, 10 fois l'opérateur *,

13
00:00:24,216 --> 00:00:26,064
ou 5 fois l'opérateur *,

14
00:00:26,366 --> 00:00:27,590
ou 1 fois, et cætera.

15
00:00:27,910 --> 00:00:29,822
Donc en fait, je choisis les opérateurs

16
00:00:30,086 --> 00:00:31,769
et en choisissant ces opérateurs,

17
00:00:32,142 --> 00:00:33,475
je suis quand même un peu malin

18
00:00:33,900 --> 00:00:34,964
et j'anticipe les choses,

19
00:00:35,064 --> 00:00:36,905
je me dis que l'implémentation

20
00:00:37,005 --> 00:00:38,228
dans un langage de programmation

21
00:00:38,328 --> 00:00:39,255
de cet opérateur

22
00:00:39,355 --> 00:00:40,479
risque d'être coûteuse.

23
00:00:41,180 --> 00:00:43,580
Et donc j'essaie d'intuiter

24
00:00:43,680 --> 00:00:45,444
et d'avoir une idée

25
00:00:45,544 --> 00:00:47,957
de la machine sur laquelle cela va s'exécuter.

26
00:00:49,556 --> 00:00:51,951
Par exemple, Retourne-prefixe était une opération

27
00:00:52,051 --> 00:00:54,918
qu'on supposait coûteuse sur une machine.

28
00:00:55,274 --> 00:00:58,253
Et donc, le coût d'un algorithme pour une donnée d,

29
00:00:58,702 --> 00:01:00,373
ça correspond au nombre d'appels

30
00:01:00,672 --> 00:01:02,769
à l'opérateur que je souhaite compter

31
00:01:03,164 --> 00:01:04,891
lors de l'exécution de l'algorithme

32
00:01:04,991 --> 00:01:06,044
sur la donnée d.

33
00:01:06,806 --> 00:01:09,147
Alors, on le sait, ça dépend de la donnée d,

34
00:01:10,128 --> 00:01:11,364
c'est peu explicatif,

35
00:01:11,464 --> 00:01:12,434
c'est-à-dire que,

36
00:01:12,534 --> 00:01:13,998
si je vous dis que mon algorithme,

37
00:01:14,098 --> 00:01:15,141
il a mis 10 étapes,

38
00:01:15,901 --> 00:01:17,792
comment je vais faire pour une autre donnée

39
00:01:19,360 --> 00:01:21,483
pour dire que ça va prendre 12 étapes.

40
00:01:22,442 --> 00:01:24,792
Une donnée, c'est sans doute pas assez.

41
00:01:25,109 --> 00:01:26,883
Et donc la question qui va se poser,

42
00:01:26,983 --> 00:01:29,020
c'est est-ce que j'ai une information sur la donnée

43
00:01:29,334 --> 00:01:30,888
et je vais essayer d'expliquer

44
00:01:30,988 --> 00:01:33,658
le nombre d'appels à l'opérateur

45
00:01:33,758 --> 00:01:35,810
en fonction d'un paramètre de la donnée.

46
00:01:36,347 --> 00:01:37,548
Et donc, typiquement,

47
00:01:37,910 --> 00:01:39,667
on va regarder la taille de la donnée.

48
00:01:39,974 --> 00:01:42,214
Par exemple, vous recherchez un élément dans un tableau,

49
00:01:42,503 --> 00:01:43,533
c'est la taille du tableau

50
00:01:43,633 --> 00:01:44,753
qui va être importante.

51
00:01:45,094 --> 00:01:46,708
Le tableau, effectivement,

52
00:01:47,393 --> 00:01:48,455
selon sa taille,

53
00:01:48,555 --> 00:01:49,748
ça va vous prendre plus ou moins de temps

54
00:01:49,848 --> 00:01:50,750
pour rechercher un élément.

55
00:01:51,323 --> 00:01:52,948
Et après, ça ne dépend plus du tableau

56
00:01:53,048 --> 00:01:54,457
mais par contre, ça dépend de la taille.

57
00:01:54,729 --> 00:01:56,772
Donc on va chercher une information

58
00:01:57,034 --> 00:01:57,798
sur les données

59
00:01:57,898 --> 00:01:59,072
qui nous semble pertinente.

60
00:01:59,327 --> 00:02:00,761
En général, en complexité,

61
00:02:00,861 --> 00:02:01,704
on parle de taille.

62
00:02:02,052 --> 00:02:03,438
Alors, ça pourrait être autre chose.

63
00:02:03,538 --> 00:02:05,179
Ça pourrait être le nombre de facteurs

64
00:02:05,279 --> 00:02:07,498
si vous travaillez avec des nombres

65
00:02:07,598 --> 00:02:09,613
et vous faites des manipulations de nombres entiers,

66
00:02:09,713 --> 00:02:10,446
ça pourrait être d'autres choses.

67
00:02:10,969 --> 00:02:12,922
Pour nous, on va appeler ça taille.

68
00:02:13,249 --> 00:02:16,197
Et donc on définit la complexité d'un algorithme

69
00:02:16,480 --> 00:02:17,992
pour une taille donnée n,

70
00:02:18,746 --> 00:02:20,516
c'est une complexité au max,

71
00:02:20,780 --> 00:02:21,954
c'est le maximum

72
00:02:22,198 --> 00:02:25,552
sur toutes les données d de taille n,

73
00:02:25,868 --> 00:02:27,745
du coût de l'algorithme A

74
00:02:27,845 --> 00:02:29,385
lors de l'exécution

75
00:02:29,485 --> 00:02:30,262
sur la donnée d.

76
00:02:31,040 --> 00:02:32,868
C'est-à-dire c'est la pire des situations

77
00:02:32,968 --> 00:02:34,399
que nous puissions emprunter.

78
00:02:35,005 --> 00:02:36,197
Alors, évidemment,

79
00:02:36,297 --> 00:02:37,389
cette complexité

80
00:02:38,528 --> 00:02:39,259
au pire,

81
00:02:39,577 --> 00:02:42,177
que souvent on appelle complexité de l'algorithme

82
00:02:42,503 --> 00:02:43,746
par abus de langage,

83
00:02:45,179 --> 00:02:46,712
cette complexité au pire,

84
00:02:47,632 --> 00:02:50,264
elle peut être différente

85
00:02:50,639 --> 00:02:51,671
selon les données.

86
00:02:51,940 --> 00:02:53,918
Tout le monde ne va pas avoir la même complexité.

87
00:02:54,194 --> 00:02:56,357
Et donc il peut être intéressant à un moment donné

88
00:02:56,631 --> 00:02:58,567
d'exhiber un exemple

89
00:02:58,821 --> 00:03:01,141
qui va vous donner cette complexité au pire.

90
00:03:01,419 --> 00:03:03,335
Donc ça, c'est une étape importante

91
00:03:03,599 --> 00:03:05,125
et sur laquelle on peut réfléchir.

92
00:03:05,574 --> 00:03:07,880
Donc en fait, quand on va travailler avec de la complexité,

93
00:03:07,980 --> 00:03:09,323
on va non seulement essayer de dire

94
00:03:10,199 --> 00:03:12,080
mon algorithme, il va coûter tant,

95
00:03:13,064 --> 00:03:15,372
mais on va essayer d'exhiber des exemples

96
00:03:15,656 --> 00:03:16,856
qui vont me coûter autant.

97
00:03:17,175 --> 00:03:18,932
Et ce qui serait encore plus intéressant

98
00:03:19,032 --> 00:03:20,442
et plus passionnant, ça serait de dire

99
00:03:20,954 --> 00:03:23,951
j'ai quelques exemples qui me coûtent très cher,

100
00:03:24,452 --> 00:03:26,005
et donc de caractériser

101
00:03:26,105 --> 00:03:27,135
l'ensemble des éléments

102
00:03:27,235 --> 00:03:28,111
qui coûtent très cher.

103
00:03:28,702 --> 00:03:31,061
Donc ça, c'est la partie analyse

104
00:03:31,614 --> 00:03:32,959
sur les cas extrêmes

105
00:03:33,521 --> 00:03:34,822
de votre algorithme, c'est-à-dire

106
00:03:34,922 --> 00:03:36,450
pousser l'algorithme dans ses retranchements

107
00:03:36,550 --> 00:03:37,515
et essayer d'améliorer

108
00:03:37,803 --> 00:03:40,197
sur les pires cas que l'on puisse observer.

109
00:03:43,361 --> 00:03:44,631
Je peux également définir

110
00:03:44,731 --> 00:03:46,768
une complexité au mieux d'un algorithme,

111
00:03:47,051 --> 00:03:48,120
c'est-à-dire dire

112
00:03:48,220 --> 00:03:50,685
sur toutes les données d que j'ai à ma disposition,

113
00:03:50,959 --> 00:03:52,134
le mieux que je puisse faire,

114
00:03:52,234 --> 00:03:54,605
c'est Cmin de n

115
00:03:54,969 --> 00:03:56,328
pour une donnée d de taille n.

116
00:03:56,804 --> 00:03:57,945
Et donc là pareil,

117
00:03:58,045 --> 00:03:58,988
il faut trouver des exemples

118
00:03:59,088 --> 00:04:00,045
et c'est tout à fait intéressant.

119
00:04:02,345 --> 00:04:03,697
J'ai la complexité au pire,

120
00:04:03,797 --> 00:04:04,921
j'ai la complexité au mieux,

121
00:04:05,153 --> 00:04:07,698
et je sais que si je prends une donnée d quelconque,

122
00:04:07,798 --> 00:04:08,448
si j'ai sa taille,

123
00:04:08,548 --> 00:04:10,349
je sais que je vais me situer entre les deux.

124
00:04:11,049 --> 00:04:12,231
Donc c'est ça l'idée

125
00:04:12,331 --> 00:04:13,968
de la complexité d'un algorithme

126
00:04:14,068 --> 00:04:15,031
en fonction de la taille.

127
00:04:15,320 --> 00:04:16,950
Ça vous permet de savoir à peu près

128
00:04:17,050 --> 00:04:18,059
où vous vous situez

129
00:04:18,463 --> 00:04:20,668
lors de l'exécution de l'algorithme

130
00:04:20,768 --> 00:04:21,703
en nombre d'appels

131
00:04:22,017 --> 00:04:24,883
ou en nombre d'exécutions de l'opérateur choisi.

132
00:04:25,182 --> 00:04:26,890
Et ça dépend toujours de cet opérateur-là.

133
00:04:29,580 --> 00:04:31,818
Alors, si on reprend le crêpier psychorigide,

134
00:04:31,918 --> 00:04:33,909
si je regarde le nombre de retournements,

135
00:04:35,369 --> 00:04:37,200
on a vu que si

136
00:04:37,300 --> 00:04:38,114
par hasard

137
00:04:40,680 --> 00:04:42,045
mon tas de crêpes était

138
00:04:43,201 --> 00:04:45,095
tel que je l'ai fait, de façon déterministe,

139
00:04:45,195 --> 00:04:45,940
et bien, j'avais

140
00:04:47,154 --> 00:04:49,130
n(n - 1) / 2 opérations

141
00:04:49,230 --> 00:04:50,603
pour retourner mon tas de crêpes.

142
00:04:51,223 --> 00:04:52,979
Donc on a quelque chose qui est de l'ordre de

143
00:04:53,079 --> 00:04:55,341
n au carré opérations.

144
00:04:55,441 --> 00:04:56,588
C'est quelque chose qui grossit

145
00:04:56,688 --> 00:04:57,760
assez vite en fonction de n,

146
00:04:59,399 --> 00:05:00,472
pas trop quand même

147
00:05:00,572 --> 00:05:01,996
mais ça grossit en fonction de n.

148
00:05:02,285 --> 00:05:03,198
Ce n'est pas linéaire,

149
00:05:03,298 --> 00:05:04,769
si vous mettez une crêpe de plus,

150
00:05:05,013 --> 00:05:06,442
vous multipliez par n en gros

151
00:05:06,771 --> 00:05:07,779
ce que vous avez à faire.

152
00:05:08,938 --> 00:05:10,921
Donc, si je fais ça,

153
00:05:11,021 --> 00:05:12,968
je peux éventuellement regarder

154
00:05:13,068 --> 00:05:14,045
des améliorations,

155
00:05:14,145 --> 00:05:16,012
pour optimiser au niveau du tas de crêpes.

156
00:05:16,311 --> 00:05:17,547
En fait, quand je fais

157
00:05:17,830 --> 00:05:20,184
mes opérations de retournement du tas de crêpes,

158
00:05:20,567 --> 00:05:21,929
et bien, je peux aussi

159
00:05:22,554 --> 00:05:23,458
vérifier

160
00:05:23,733 --> 00:05:26,012
si ma crêpe n'est pas en bonne position

161
00:05:26,112 --> 00:05:27,196
au moment où je vais retourner,

162
00:05:28,301 --> 00:05:29,450
faire mes deux retournements.

163
00:05:29,550 --> 00:05:31,114
Et dans ce cas-là, je ne fais rien.

164
00:05:31,847 --> 00:05:33,912
Donc si par exemple mon tas de crêpes

165
00:05:34,012 --> 00:05:35,909
est bien joli et bien dans l'ordre,

166
00:05:36,273 --> 00:05:38,485
je n'ai aucune opération à faire.

167
00:05:38,774 --> 00:05:40,658
Et donc, dans le cas du tas de crêpes,

168
00:05:40,758 --> 00:05:42,811
dans ce cas-là, on peut optimiser.

169
00:05:43,179 --> 00:05:45,895
On aurait au minimum zéro opération de retournement

170
00:05:45,995 --> 00:05:48,056
et au maximum n(n - 1)/2.

171
00:05:48,373 --> 00:05:49,412
Alors je vous laisse chercher

172
00:05:49,512 --> 00:05:50,675
l'exemple qui vous donnerait

173
00:05:50,775 --> 00:05:51,817
n(n - 1)/2

174
00:05:51,917 --> 00:05:53,037
qui est déterministe,

175
00:05:53,296 --> 00:05:54,795
et si ce n'est pas déterministe,

176
00:05:57,355 --> 00:05:59,005
vous pouvez trouver d'autres exemples.

177
00:06:01,065 --> 00:06:05,604
Continuons avec notre calcul de puissance.

178
00:06:05,903 --> 00:06:07,615
Si je prends le calcul de puissance,

179
00:06:07,715 --> 00:06:08,550
en fait, vous voyez,

180
00:06:08,650 --> 00:06:09,650
vous faites exactement

181
00:06:09,750 --> 00:06:10,600
n itérations.

182
00:06:12,399 --> 00:06:13,359
Donc ici,

183
00:06:13,459 --> 00:06:15,233
vous avez exactement n opérations

184
00:06:15,651 --> 00:06:17,863
et donc la complexité dans ce cas-là

185
00:06:17,963 --> 00:06:19,312
ne dépend que

186
00:06:19,682 --> 00:06:20,696
de la taille des données.

187
00:06:20,796 --> 00:06:22,485
Ceci, ça permet de dire

188
00:06:23,124 --> 00:06:25,682
dans mon itération, j'en ai n

189
00:06:26,079 --> 00:06:29,013
et je vais pouvoir essayer d'améliorer ça,

190
00:06:29,113 --> 00:06:30,578
voir si ça marche, si ça ne marche pas.

191
00:06:30,944 --> 00:06:32,040
On va voir un peu plus loin

192
00:06:32,140 --> 00:06:33,776
comment ça peut fonctionner.

193
00:06:33,876 --> 00:06:36,081
Donc si je veux évaluer

194
00:06:36,181 --> 00:06:38,095
la complexité d'un algorithme,

195
00:06:38,800 --> 00:06:40,005
je peux me poser la question :

196
00:06:40,105 --> 00:06:42,273
voilà, est-ce que j'ai des méthodes simples

197
00:06:42,373 --> 00:06:44,252
pour évaluer la complexité d'un algorithme ?

198
00:06:44,590 --> 00:06:45,742
La réponse est oui.

199
00:06:46,085 --> 00:06:47,388
Et ça se fait à la construction,

200
00:06:47,488 --> 00:06:48,572
c'est-à-dire que ça va être

201
00:06:48,672 --> 00:06:51,048
au fur et à mesure que l'on avance

202
00:06:51,333 --> 00:06:53,141
dans la construction de l'algorithme

203
00:06:53,241 --> 00:06:54,798
qu'on va pouvoir regarder

204
00:06:54,898 --> 00:06:56,504
quelle est la complexité qui est associée.

205
00:06:56,828 --> 00:06:58,612
Donc en fait, c'est intimement lié

206
00:06:58,712 --> 00:07:00,018
à l'écriture de l'algorithme,

207
00:07:00,118 --> 00:07:01,861
de la façon dont vous allez faire les choses.

208
00:07:02,163 --> 00:07:03,271
On n'écrit pas un algorithme

209
00:07:03,371 --> 00:07:05,025
et on évalue sa complexité après.

210
00:07:05,125 --> 00:07:06,503
On fait les deux choses en même temps.

211
00:07:06,603 --> 00:07:07,966
On essaie de réfléchir en même temps

212
00:07:08,066 --> 00:07:09,321
qu'on écrit son algorithme

213
00:07:09,421 --> 00:07:10,714
aux coûts qui sont engendrés.

