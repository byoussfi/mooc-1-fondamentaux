1
00:00:00,954 --> 00:00:04,411
Pour finir cette partie sur la preuve,

2
00:00:05,531 --> 00:00:08,086
on va réfléchir à ce qu'il faut faire

3
00:00:08,392 --> 00:00:09,458
en matière de preuve.

4
00:00:10,208 --> 00:00:12,918
Il y a deux choses à vérifier en matière de preuve,

5
00:00:13,018 --> 00:00:14,900
la correction partielle que l'on vient de voir,

6
00:00:15,000 --> 00:00:17,701
en particulier avec une illustration

7
00:00:17,801 --> 00:00:19,695
sur la notion d'invariant,

8
00:00:20,035 --> 00:00:23,292
il faut donc que pour toute donnée D

9
00:00:23,392 --> 00:00:25,124
qui vérifie la précondition P,

10
00:00:25,224 --> 00:00:26,644
si le programme se termine,

11
00:00:26,744 --> 00:00:28,581
alors le résultat R

12
00:00:28,681 --> 00:00:30,566
vérifie la postcondition Q.

13
00:00:31,020 --> 00:00:32,363
Et la correction totale

14
00:00:32,618 --> 00:00:34,630
implique qu'il faut

15
00:00:34,730 --> 00:00:38,129
que l'algorithme se termine,

16
00:00:38,229 --> 00:00:40,248
c'est-à-dire que l'on a effectivement

17
00:00:40,348 --> 00:00:42,455
un nombre fini d'opérations

18
00:00:42,555 --> 00:00:44,178
à l'intérieur de notre algorithme.

19
00:00:45,281 --> 00:00:48,106
Alors, cette correction totale,

20
00:00:48,498 --> 00:00:50,562
c'est vraiment la chose importante.

21
00:00:50,662 --> 00:00:52,695
Alors il faut faire très attention

22
00:00:52,795 --> 00:00:54,174
parce que là, c'est un choix

23
00:00:54,274 --> 00:00:56,432
en matière de preuve d'algorithme,

24
00:00:56,673 --> 00:00:59,033
de dire correction partielle, correction totale.

25
00:00:59,270 --> 00:01:02,013
C'est deux choses qu'on a séparées

26
00:01:02,337 --> 00:01:04,384
parce qu'en fait, on avait à séparer

27
00:01:04,484 --> 00:01:05,759
le problème des boucles

28
00:01:05,859 --> 00:01:08,038
et des itérations qui étaient infinies

29
00:01:08,138 --> 00:01:09,356
qui étaient difficiles à prouver

30
00:01:09,456 --> 00:01:11,851
avec la propagation des assertions.

31
00:01:11,951 --> 00:01:13,841
Donc c'est intimement lié,

32
00:01:13,941 --> 00:01:14,846
ces deux choses-là,

33
00:01:14,946 --> 00:01:17,225
l'une à la dynamique du programme,

34
00:01:17,325 --> 00:01:18,836
et l'autre à l'état des variables.

35
00:01:19,168 --> 00:01:21,640
Ce sont deux choses qui sont orthogonales.

36
00:01:22,221 --> 00:01:25,008
Comment est-ce qu'on va démontrer

37
00:01:25,108 --> 00:01:26,983
qu'une boucle se termine ?

38
00:01:27,217 --> 00:01:28,699
Et bien on utilise,

39
00:01:28,799 --> 00:01:30,603
ça ne marche pas forcément tout le temps,

40
00:01:30,919 --> 00:01:32,513
la notion de variant de boucle.

41
00:01:32,753 --> 00:01:34,414
Un variant de boucle, c'est quoi ?

42
00:01:34,514 --> 00:01:35,497
C'est une expression,

43
00:01:35,776 --> 00:01:37,287
alors ça peut être un entier,

44
00:01:37,387 --> 00:01:38,863
ça peut être une taille,

45
00:01:38,963 --> 00:01:41,577
ça peut être ce que vous voulez,

46
00:01:41,858 --> 00:01:43,557
mais quelque chose en général

47
00:01:43,657 --> 00:01:45,011
de discret,

48
00:01:45,708 --> 00:01:47,346
qui est positif,

49
00:01:47,446 --> 00:01:48,921
et qui décroît strictement

50
00:01:49,021 --> 00:01:50,163
à chacune des étapes,

51
00:01:50,263 --> 00:01:51,435
à chacune des itérations.

52
00:01:52,299 --> 00:01:55,327
Les variants usuels que vous pouvez utiliser,

53
00:01:55,427 --> 00:01:56,823
c'est donc le variant de boucle

54
00:01:57,119 --> 00:02:00,663
si vous avez une boucle qui va de n à 1,

55
00:02:00,976 --> 00:02:03,497
et bien vous utilisez la valeur de i comme variant,

56
00:02:03,597 --> 00:02:06,397
si vous avez une boucle qui va de 1 à n,

57
00:02:06,497 --> 00:02:07,996
vous utilisez n - i,

58
00:02:08,280 --> 00:02:11,469
j - i si vous avez deux variables, et cætera.

59
00:02:11,569 --> 00:02:13,178
Mais ce n'est pas systématique

60
00:02:13,278 --> 00:02:14,806
et on est très embêtés

61
00:02:14,906 --> 00:02:16,258
parce que souvent,

62
00:02:17,283 --> 00:02:19,813
c'est compliqué de construire le bon variant,

63
00:02:19,913 --> 00:02:21,150
celui qui va bien décroître.

64
00:02:21,439 --> 00:02:23,482
Il faut que cela décroisse strictement

65
00:02:23,791 --> 00:02:28,029
et donc on utilise quelque chose qui est un principe,

66
00:02:28,129 --> 00:02:30,747
si vous avez une suite discrète

67
00:02:30,847 --> 00:02:32,802
qui est strictement décroissante

68
00:02:32,902 --> 00:02:34,677
bornée inférieurement,

69
00:02:34,777 --> 00:02:39,594
à condition que vous ayez un nombre fini

70
00:02:39,694 --> 00:02:41,012
de pas possibles,

71
00:02:41,112 --> 00:02:42,836
dans ce cas-là, ça se termine.

72
00:02:42,936 --> 00:02:44,955
Dans la plupart des cas que l'on regarde,

73
00:02:45,055 --> 00:02:46,221
ça va marcher.

74
00:02:46,321 --> 00:02:47,968
Et si vous faites ça avec éventuellement

75
00:02:48,068 --> 00:02:49,236
des nombres réels ou des choses comme ça,

76
00:02:49,336 --> 00:02:50,531
des fois, ça peut ne pas fonctionner.

77
00:02:51,226 --> 00:02:52,274
Donc attention.

78
00:02:53,312 --> 00:02:55,344
Si je reprends ma division par soustractions,

79
00:02:55,622 --> 00:02:57,430
j'ai mes deux entiers, et cætera,

80
00:02:57,745 --> 00:02:59,197
while r est plus grand que b,

81
00:02:59,297 --> 00:03:01,344
j'ai un variant qui est immédiat,

82
00:03:01,444 --> 00:03:03,484
la valeur de r,

83
00:03:03,584 --> 00:03:04,952
à chaque fois je lui enlève quelque chose,

84
00:03:05,264 --> 00:03:09,176
donc la valeur de r est toujours décroissante strictement

85
00:03:09,473 --> 00:03:11,170
et elle est toujours positive,

86
00:03:11,270 --> 00:03:14,595
on l'a vu quand on l'a utilisée dans un invariant d'itération

87
00:03:14,695 --> 00:03:15,362
au tout début,

88
00:03:15,462 --> 00:03:17,595
et donc, ça va finir par s'arrêter.

89
00:03:17,916 --> 00:03:18,844
Je n'ai pas besoin de savoir

90
00:03:18,944 --> 00:03:20,172
combien de pas ça va prendre,

91
00:03:20,272 --> 00:03:22,726
ça, c'est l'analyse de coût qui va vous le donner,

92
00:03:22,826 --> 00:03:24,821
ici, j'ai juste besoin de savoir que ça s'arrête.

93
00:03:28,217 --> 00:03:29,818
Il est positif,

94
00:03:29,918 --> 00:03:31,236
il est strictement décroissant,

95
00:03:31,664 --> 00:03:32,593
donc ça s'arrête.

96
00:03:32,693 --> 00:03:34,384
Ça suffit à montrer que

97
00:03:34,484 --> 00:03:36,200
l'itération se termine.

98
00:03:36,520 --> 00:03:38,111
L'itération se termine,

99
00:03:38,347 --> 00:03:40,030
la postcondition est vérifiée

100
00:03:40,130 --> 00:03:41,380
donc mon algorithme est correct.

101
00:03:42,877 --> 00:03:44,765
Quelles sont les difficultés qu'on a

102
00:03:44,865 --> 00:03:46,879
quand on veut faire de la preuve d'algorithme ?

103
00:03:47,336 --> 00:03:51,295
Les difficultés sont de natures assez différentes.

104
00:03:53,175 --> 00:03:54,523
Moi, je pense que

105
00:03:54,623 --> 00:03:56,967
la principale difficulté, c'est de bien comprendre

106
00:03:57,216 --> 00:03:59,143
comment fonctionne l'algorithme,

107
00:03:59,612 --> 00:04:02,564
écrire les préconditions et les postconditions.

108
00:04:04,403 --> 00:04:07,672
Si on n'a pas fait l'effort de bien spécifier au départ,

109
00:04:07,772 --> 00:04:09,032
évidemment, ce n'est pas possible,

110
00:04:09,332 --> 00:04:10,875
donc il y a aussi un effort

111
00:04:10,975 --> 00:04:12,659
de bien décrire ce que l'on a

112
00:04:12,923 --> 00:04:14,128
au début et à la fin.

113
00:04:14,863 --> 00:04:16,160
On peut éventuellement

114
00:04:16,260 --> 00:04:19,366
partir sur des préconditions qui sont plus contraignantes

115
00:04:19,653 --> 00:04:21,750
et à la fin, regarder si on peut avoir

116
00:04:21,850 --> 00:04:23,373
des préconditions plus faibles

117
00:04:23,622 --> 00:04:26,662
qui permettent de prouver l'algorithme.

118
00:04:27,157 --> 00:04:29,029
Si vous n'arrivez pas à prouver l'algorithme

119
00:04:29,129 --> 00:04:30,053
avec une précondition,

120
00:04:30,153 --> 00:04:31,580
c'est peut-être qu'elle n'est pas assez forte.

121
00:04:31,680 --> 00:04:32,617
Dans ce cas-là,

122
00:04:32,775 --> 00:04:35,996
vous pouvez changer la précondition.

123
00:04:37,401 --> 00:04:38,954
Et puis vous pouvez aussi

124
00:04:39,171 --> 00:04:40,759
vérifier que la postcondition,

125
00:04:40,859 --> 00:04:42,338
des fois, elle n'est pas suffisante.

126
00:04:43,617 --> 00:04:45,428
Par exemple, on s'est rendu compte qu'il fallait avoir

127
00:04:45,528 --> 00:04:47,454
r supérieur ou égal à 0.

128
00:04:47,554 --> 00:04:48,950
L'invariant qu'on avait fait

129
00:04:49,050 --> 00:04:50,038
ne marchait pas,

130
00:04:50,138 --> 00:04:52,461
et si on avait peut-être a négatif,

131
00:04:52,916 --> 00:04:54,359
ça n'aurait peut-être pas marché.

132
00:04:56,389 --> 00:04:59,071
Donc les invariants qu'on a à construire

133
00:04:59,171 --> 00:05:02,118
sont basés sur ces propriétés des valeurs des variables.

134
00:05:02,695 --> 00:05:05,473
C'est souvent quelque chose de plus fort

135
00:05:05,573 --> 00:05:07,855
que la postcondition dont vous avez besoin à la fin.

136
00:05:09,626 --> 00:05:13,136
Selon les écoles,

137
00:05:13,567 --> 00:05:15,096
il y a des collègues qui disent

138
00:05:15,196 --> 00:05:17,152
qu'on commence par écrire les invariants,

139
00:05:17,615 --> 00:05:19,327
on commence par écrire les propriétés,

140
00:05:19,427 --> 00:05:21,556
avant d'écrire le programme.

141
00:05:23,653 --> 00:05:25,931
Les variants, c'est souvent plus simple.

142
00:05:26,420 --> 00:05:28,614
C'est souvent immédiat si vous avez des itérations

143
00:05:28,964 --> 00:05:30,203
mais de temps en temps,

144
00:05:30,303 --> 00:05:32,274
on a des cas qui sont plus difficiles.

145
00:05:32,556 --> 00:05:34,571
La suite de Syracuse est un exemple

146
00:05:34,671 --> 00:05:36,517
où on n'a pas de variant

147
00:05:36,617 --> 00:05:38,425
et on n'est pas sûr que l'algorithme se termine.

148
00:05:42,703 --> 00:05:44,014
Pour résumer,

149
00:05:44,292 --> 00:05:45,926
un algorithme correct,

150
00:05:46,026 --> 00:05:48,635
c'est toujours par rapport à une spécification,

151
00:05:49,558 --> 00:05:51,862
un invariant, c'est une propriété

152
00:05:51,962 --> 00:05:53,796
qui est préservée par une boucle,

153
00:05:54,757 --> 00:05:57,888
un variant, c'est une quantité décroissante

154
00:05:57,988 --> 00:05:59,530
à chaque itération

155
00:05:59,882 --> 00:06:01,295
et assure la terminaison.

156
00:06:01,395 --> 00:06:02,868
Une fois qu'on a fait tout ça,

157
00:06:02,968 --> 00:06:04,270
on pourrait se poser la question de savoir si

158
00:06:05,831 --> 00:06:07,024
on n'a pas d'autre manière

159
00:06:07,124 --> 00:06:08,801
de montrer la correction d'un programme,

160
00:06:09,073 --> 00:06:14,258
et on pourrait effectivement avoir d'autres façons de faire,

161
00:06:14,867 --> 00:06:16,125
il y a des méthodes automatiques

162
00:06:16,225 --> 00:06:17,452
pour corriger des programmes,

163
00:06:17,724 --> 00:06:18,994
il y a des solveurs

164
00:06:19,094 --> 00:06:22,359
et des prouveurs de programmes

165
00:06:22,459 --> 00:06:24,713
qui permettent de démontrer effectivement un algorithme.

166
00:06:25,859 --> 00:06:28,194
On peut aussi se dire je fais des tests,

167
00:06:28,458 --> 00:06:29,737
c'est-à-dire je vais tester

168
00:06:29,837 --> 00:06:31,144
si mon algorithme est correct.

169
00:06:31,477 --> 00:06:33,681
Alors un test ne va être valide

170
00:06:33,781 --> 00:06:36,336
que si vous le vérifiez pour toutes les données en entrée.

171
00:06:36,436 --> 00:06:39,860
N'oubliez pas qu'il y a quelle que soit la donnée en entrée.

172
00:06:40,133 --> 00:06:43,711
Pour toute donnée, vous pouvez effectivement tester

173
00:06:43,811 --> 00:06:45,471
mais malheureusement, ça ne marche pas toujours,

174
00:06:46,151 --> 00:06:47,349
c'est beaucoup trop gros

175
00:06:47,449 --> 00:06:49,340
si vous avez à tester toutes les permutations

176
00:06:49,440 --> 00:06:51,145
pour vérifier que votre tri est correct,

177
00:06:51,245 --> 00:06:54,191
vous en aurez pour quelques années de calcul.

178
00:06:54,507 --> 00:06:56,899
Et donc, faire des tests

179
00:06:56,999 --> 00:06:59,303
en disant je choisis des données et je teste,

180
00:06:59,403 --> 00:07:02,762
ça ne va pas vous démontrer que votre algorithme est correct.

181
00:07:03,541 --> 00:07:07,235
Par contre, ça va vous illustrer le fait que

182
00:07:07,583 --> 00:07:10,081
vous pouvez faire remonter des bugs

183
00:07:10,388 --> 00:07:12,769
quand vous allez tester un algorithme.

184
00:07:13,142 --> 00:07:16,536
Ça, c'est une phrase que j'aime bien de Dijkstra,

185
00:07:16,636 --> 00:07:18,162
qui dit que si

186
00:07:18,262 --> 00:07:20,237
vous avez un programme qui teste

187
00:07:20,337 --> 00:07:22,447
il va vous faire remonter des bugs,

188
00:07:22,547 --> 00:07:24,115
il ne va rien vous dire

189
00:07:24,215 --> 00:07:25,891
sur le fait qu'il n'y a pas de bug

190
00:07:25,991 --> 00:07:27,138
dans votre algorithme.

191
00:07:28,776 --> 00:07:32,548
Que fait en fait le test derrière ?

192
00:07:32,648 --> 00:07:35,519
Il va juste valider une implémentation

193
00:07:35,619 --> 00:07:37,036
de façon partielle

194
00:07:38,583 --> 00:07:39,524
d'un algorithme,

195
00:07:39,624 --> 00:07:41,322
puisque vous allez tester sur un programme.

196
00:07:42,021 --> 00:07:44,020
Vous pouvez éliminer quelques bugs,

197
00:07:44,120 --> 00:07:45,214
ça, c'est facile.

198
00:07:45,516 --> 00:07:47,622
Et puis, vous le faites en cours de développement.

199
00:07:47,927 --> 00:07:50,208
Alors, ce dont on n'a pas parlé,

200
00:07:50,422 --> 00:07:52,787
c'est effectivement comment on génère des tests

201
00:07:53,208 --> 00:07:55,475
et la génération des tests, elle,

202
00:07:55,575 --> 00:07:57,660
peut se faire directement à partir de l'algorithme,

203
00:07:57,991 --> 00:08:00,394
c'est-à-dire je regarde les propriétés de l'algorithme

204
00:08:00,494 --> 00:08:01,555
et je génère des tests.

205
00:08:02,279 --> 00:08:04,307
La preuve, elle, telle qu'on vient de la faire,

206
00:08:04,407 --> 00:08:07,013
garantit de façon incontestable

207
00:08:07,113 --> 00:08:10,683
que la méthode, le fond de l'algorithme est correct.

208
00:08:11,050 --> 00:08:14,000
Vous n'éliminez pas les erreurs d'implémentation,

209
00:08:14,100 --> 00:08:16,027
vous n'éliminez pas les erreurs de programmation,

210
00:08:16,311 --> 00:08:18,879
vous n'éliminez pas les erreurs d'ambiguïté

211
00:08:18,979 --> 00:08:21,509
que vous pouvez avoir sur les préconditions sur les données

212
00:08:22,009 --> 00:08:24,715
et donc vous avez un outil qui vous permet

213
00:08:24,994 --> 00:08:27,344
de, je dirais, garantir

214
00:08:27,600 --> 00:08:28,675
à un certain niveau

215
00:08:29,014 --> 00:08:30,667
les propriétés de votre algorithme.

