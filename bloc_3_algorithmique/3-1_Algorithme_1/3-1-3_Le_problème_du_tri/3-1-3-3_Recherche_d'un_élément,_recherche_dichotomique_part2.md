## Recherche d'un élément, recherche dichotomique (itératif récursif, (diviser pour régner)

1. Présentation
2. **Recherche d'un élément, recherche dichotomique (itératif récursif, (diviser pour régner)**
3. Le problème du tri : contraintes, propriétés attendues
4. Le tri par sélection (preuve, complexité)
5. Le tri par insertion (preuve, complexité)
6. Autres tris (bulle,...)

[![Vidéo 3 B3-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S3-video4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S3-video4.mp4)


[![Vidéo 4 B3-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S3-video5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S3-video5.mp4)


## Présentation

[Support de la présentation des vidéos 2 à 5](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/__supports-de-cours/B3-M1/B3-M1-S3-part1.pdf)
