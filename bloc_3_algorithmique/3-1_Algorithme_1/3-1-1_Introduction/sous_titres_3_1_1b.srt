1
00:00:00,896 --> 00:00:04,291
En fait, quand on veut définir un algorithme,

2
00:00:04,391 --> 00:00:07,795
on se trouve face à une certaine difficulté

3
00:00:07,895 --> 00:00:10,531
et pédagogiquement, dans bon nombre d'ouvrages,

4
00:00:10,631 --> 00:00:13,959
les gens se disent : je vais chercher une analogie

5
00:00:14,431 --> 00:00:16,608
pour essayer d'expliquer avec des mots de la vie courante

6
00:00:17,016 --> 00:00:18,369
ce que c'est qu'un algorithme.

7
00:00:18,786 --> 00:00:21,060
L'analogie la plus traditionnelle

8
00:00:21,160 --> 00:00:22,043
qui est employée,

9
00:00:22,143 --> 00:00:24,850
c'est ce qu'on appelle l'analogie de la recette de cuisine.

10
00:00:25,144 --> 00:00:27,056
Ce que je vous propose maintenant,

11
00:00:27,156 --> 00:00:28,789
histoire de vous faire saliver un peu,

12
00:00:29,161 --> 00:00:30,700
c'est de prendre une recette de cuisine

13
00:00:30,800 --> 00:00:32,436
et de regarder, de travailler

14
00:00:32,536 --> 00:00:34,134
sur l'analogie correspondante.

15
00:00:34,502 --> 00:00:37,112
Donc si je regarde une recette de cuisine,

16
00:00:37,747 --> 00:00:41,364
ici, c'est un extrait d'une recette de pâtisserie,

17
00:00:43,004 --> 00:00:44,648
donc fabrication de madeleines.

18
00:00:44,748 --> 00:00:48,793
L'idée est de se faire douze madeleines

19
00:00:49,331 --> 00:00:50,472
au four, et cætera.

20
00:00:51,290 --> 00:00:54,392
On est quand même intéressés par la gourmandise.

21
00:00:54,865 --> 00:00:57,229
Et donc maintenant, je vais reprendre une recette de pâtisserie,

22
00:00:57,329 --> 00:01:00,661
elle est extraite du Larousse des Desserts de Pierre Hermé,

23
00:01:01,356 --> 00:01:03,404
et si je reprends cette recette de cuisine,

24
00:01:03,504 --> 00:01:05,000
je regarde cette recette de cuisine,

25
00:01:05,400 --> 00:01:06,409
j'ai d'abord

26
00:01:09,213 --> 00:01:11,496
quelque chose d'important.

27
00:01:11,939 --> 00:01:16,357
Ici, j'ai ce que j'appelle l'objectif

28
00:01:16,778 --> 00:01:19,610
c'est-à-dire que je voudrais produire

29
00:01:20,143 --> 00:01:21,785
une douzaine de madeleines,

30
00:01:21,885 --> 00:01:23,151
et pour faire ça,

31
00:01:23,251 --> 00:01:24,927
et bien je vais avoir

32
00:01:25,027 --> 00:01:27,671
quelque part un certain nombre d'entrées,

33
00:01:28,528 --> 00:01:32,085
donc on pourrait dire que ça, c'est les entrées.

34
00:01:34,239 --> 00:01:35,648
J'ai un certain nombre d'entrées

35
00:01:35,748 --> 00:01:36,886
qui correspondent

36
00:01:39,239 --> 00:01:43,030
à ce que j'ai à ma disposition au départ.

37
00:01:43,437 --> 00:01:46,573
Et puis, je vais avoir toute une série d'opérations.

38
00:01:47,005 --> 00:01:48,869
Donc ici, j'ai des opérations

39
00:01:49,300 --> 00:01:50,982
qui sont indiquées avec

40
00:01:51,082 --> 00:01:52,335
des numéros.

41
00:01:53,650 --> 00:01:55,567
Donc ici, j'ai une des opérations.

42
00:01:56,791 --> 00:01:59,506
Alors, il faut bien comprendre que ces opérations,

43
00:01:59,869 --> 00:02:01,223
elles ont l'air de s'enchaîner

44
00:02:01,323 --> 00:02:04,676
mais finalement, elles ne s'enchaînent pas tout à fait

45
00:02:05,172 --> 00:02:08,181
puisqu'on peut faire des choses en parallèle

46
00:02:08,281 --> 00:02:09,638
éventuellement dans la recette.

47
00:02:10,230 --> 00:02:11,838
Donc une fois qu'on a vu ça,

48
00:02:11,938 --> 00:02:14,524
on se dit : tiens, comment est-ce qu'il a présenté

49
00:02:14,624 --> 00:02:16,564
sa recette ?

50
00:02:16,959 --> 00:02:20,590
Alors, c'est quelqu'un qui est assez,

51
00:02:20,690 --> 00:02:23,083
c'est un livre de pâtisserie qui est assez avancé,

52
00:02:23,183 --> 00:02:24,288
c'est-à-dire que

53
00:02:24,388 --> 00:02:26,700
il suppose de la part de son lecteur

54
00:02:27,438 --> 00:02:30,030
un certain nombre de connaissances

55
00:02:30,130 --> 00:02:31,305
et c'est ça qui est important,

56
00:02:31,405 --> 00:02:32,968
des connaissances en pâtisserie

57
00:02:33,068 --> 00:02:34,195
sur des termes.

58
00:02:34,295 --> 00:02:35,451
Par exemple,

59
00:02:35,551 --> 00:02:37,853
vous savez ce que c'est que tamiser,

60
00:02:40,401 --> 00:02:43,384
"tamisez la farine et la levure".

61
00:02:43,733 --> 00:02:45,655
Vous savez ce que c'est que

62
00:02:45,755 --> 00:02:47,310
faire un mélange

63
00:02:47,410 --> 00:02:50,665
farine-levure pour faire mousser.

64
00:02:51,085 --> 00:02:51,972
par exemple,

65
00:02:52,383 --> 00:02:53,678
"faire mousser" qui est ici.

66
00:02:54,058 --> 00:02:56,618
Un mélange farine-levure en pluie,

67
00:02:56,718 --> 00:02:58,646
c'est quoi, en pluie avec farine-levure ?

68
00:02:58,746 --> 00:03:00,405
Donc tout ça, ce sont des termes

69
00:03:00,505 --> 00:03:01,959
qu'utilise le pâtissier

70
00:03:02,113 --> 00:03:04,319
et qui font référence à un langage commun

71
00:03:04,843 --> 00:03:06,048
que les gens connaissent,

72
00:03:06,148 --> 00:03:07,003
et qu'ils savent faire.

73
00:03:07,419 --> 00:03:08,398
Pour faire ça,

74
00:03:08,600 --> 00:03:10,745
effectivement, c'est un certain nombre d'opérations,

75
00:03:10,900 --> 00:03:13,901
il a besoin d'un certain nombre de ressources

76
00:03:14,001 --> 00:03:16,060
pour faire son travail.

77
00:03:16,400 --> 00:03:17,883
Alors, qu'est-ce que c'est que les ressources ?

78
00:03:18,196 --> 00:03:20,245
Donc les ressources,

79
00:03:20,345 --> 00:03:21,541
ce sont les objets

80
00:03:21,641 --> 00:03:23,040
dont vous avez besoin

81
00:03:23,140 --> 00:03:24,262
pour effectivement

82
00:03:24,362 --> 00:03:26,487
faire réaliser vos madeleines.

83
00:03:26,587 --> 00:03:27,951
Donc il n'y a pas que l'huile de coude,

84
00:03:28,260 --> 00:03:29,671
vous avez besoin

85
00:03:29,771 --> 00:03:30,909
d'un four,

86
00:03:31,371 --> 00:03:32,833
vous avez besoin

87
00:03:32,933 --> 00:03:34,355
d'une plaque à madeleines,

88
00:03:34,553 --> 00:03:35,866
vous avez besoin

89
00:03:37,867 --> 00:03:40,129
d'un bol, par exemple, et cætera.

90
00:03:40,229 --> 00:03:42,237
Donc on a, à notre disposition,

91
00:03:42,337 --> 00:03:44,051
et ça, c'est sous-jacent

92
00:03:44,151 --> 00:03:45,976
à tous les algorithmes,

93
00:03:46,076 --> 00:03:47,654
on a à notre disposition

94
00:03:47,754 --> 00:03:49,640
un certain nombre de matériels

95
00:03:50,077 --> 00:03:52,709
et un certain nombre d'opérations qui nous permettent

96
00:03:52,809 --> 00:03:54,104
d'utiliser ce matériel.

97
00:03:54,552 --> 00:03:57,444
Donc ça, c'est vraiment la chose importante à retenir,

98
00:04:00,744 --> 00:04:02,585
sur la recette de cuisine.

99
00:04:03,025 --> 00:04:05,216
Donc ça, c'est la partie, je dirais,

100
00:04:05,316 --> 00:04:07,262
de description de l'environnement

101
00:04:07,362 --> 00:04:10,495
dans lequel vous allez exécuter votre recette.

102
00:04:10,870 --> 00:04:13,093
Maintenant, il y a d'autres facteurs qui vont intervenir

103
00:04:13,193 --> 00:04:15,461
qui sont tout à fait intéressants,

104
00:04:15,561 --> 00:04:16,907
ce sont des paramètres de ressources

105
00:04:17,388 --> 00:04:19,306
et qui ne sont pas forcément explicites.

106
00:04:20,374 --> 00:04:22,263
Ces autres ressources que vous avez,

107
00:04:22,692 --> 00:04:23,919
c'est par exemple

108
00:04:25,627 --> 00:04:26,727
ces ressources-là.

109
00:04:27,161 --> 00:04:28,495
Celles-là,

110
00:04:29,789 --> 00:04:30,700
celle-là.

111
00:04:34,194 --> 00:04:35,658
Donc ces ressources-là,

112
00:04:35,758 --> 00:04:37,049
ce sont des ressources en temps.

113
00:04:37,397 --> 00:04:38,887
C'est-à-dire que on va

114
00:04:38,987 --> 00:04:41,398
agrémenter notre recette de cuisine

115
00:04:41,853 --> 00:04:43,955
d'un certain nombre d'informations

116
00:04:44,374 --> 00:04:46,444
disant combien de temps doit durer

117
00:04:46,544 --> 00:04:47,646
telle ou telle opération.

118
00:04:48,216 --> 00:04:49,843
Et ça, c'est quand même quelque chose

119
00:04:49,943 --> 00:04:51,051
d'important à noter,

120
00:04:51,522 --> 00:04:53,542
et le temps va jouer un rôle

121
00:04:54,649 --> 00:04:55,591
très important

122
00:04:56,774 --> 00:04:58,143
dans tout ce qui est informatique

123
00:04:58,243 --> 00:04:59,378
puisque ça va correspondre

124
00:04:59,478 --> 00:05:01,184
au déroulement de l'exécution

125
00:05:01,284 --> 00:05:02,581
d'un programme ou d'un algorithme.

126
00:05:03,461 --> 00:05:05,210
Et donc, on a ici

127
00:05:05,310 --> 00:05:06,510
une première description,

128
00:05:06,610 --> 00:05:08,916
une première analogie pour le faire,

129
00:05:09,500 --> 00:05:11,631
mais il y a quand même deux petites choses

130
00:05:11,731 --> 00:05:13,013
qu'il faut encore rajouter.

131
00:05:15,746 --> 00:05:18,219
Pierre Hermé, quand il a écrit sa recette,

132
00:05:20,656 --> 00:05:22,893
il ne l'a pas écrite

133
00:05:22,993 --> 00:05:24,110
juste pour 12 madeleines,

134
00:05:24,583 --> 00:05:26,795
en fait, il l'a écrite pour que,

135
00:05:26,895 --> 00:05:28,372
si vous avez envie de faire

136
00:05:28,932 --> 00:05:30,107
24 madeleines,

137
00:05:30,207 --> 00:05:31,314
et bien, c'est facile,

138
00:05:31,414 --> 00:05:33,752
il suffit de doubler toutes les quantités.

139
00:05:34,290 --> 00:05:37,288
Donc ce que vous avez qui est important qui est ici,

140
00:05:38,000 --> 00:05:39,474
qui est donné dans la recette,

141
00:05:39,859 --> 00:05:41,687
c'est cette chose-là

142
00:05:42,137 --> 00:05:44,472
qui en fait vous donne

143
00:05:45,100 --> 00:05:47,358
le nombre de madeleines que vous avez en sortie.

144
00:05:47,747 --> 00:05:49,182
Donc en fait, ça,

145
00:05:49,516 --> 00:05:51,593
ça va jouer le rôle de paramètre

146
00:05:51,693 --> 00:05:52,875
de votre recette.

147
00:05:52,975 --> 00:05:54,677
Si vous avez envie d'avoir

148
00:05:54,777 --> 00:05:55,945
24 madeleines,

149
00:05:56,045 --> 00:05:58,594
et bien vous multipliez par 2 toutes les quantités.

150
00:05:58,694 --> 00:06:00,567
Si vous avez envie d'avoir 6 madeleines,

151
00:06:00,667 --> 00:06:02,310
il vous suffit de diviser par 2

152
00:06:04,213 --> 00:06:05,997
toutes les quantités.

153
00:06:07,477 --> 00:06:09,948
Donc on a effectivement quelque chose

154
00:06:10,048 --> 00:06:11,404
qui joue le rôle de paramètre

155
00:06:11,504 --> 00:06:13,790
et donc un algorithme, vous pouvez le voir

156
00:06:13,890 --> 00:06:16,385
comme une "fonction impérative",

157
00:06:16,485 --> 00:06:18,394
en "fonction paramétrée",

158
00:06:18,494 --> 00:06:20,254
qui va vous produire quelque chose.

159
00:06:20,733 --> 00:06:22,718
Alors attention, une fonction vous donne un résultat,

160
00:06:22,819 --> 00:06:24,750
ici, vous changez quand même

161
00:06:24,850 --> 00:06:27,350
l'état du beurre, des œufs, et cætera,

162
00:06:27,450 --> 00:06:29,209
donc il y a quelque chose d'un peu plus complexe

163
00:06:29,309 --> 00:06:30,220
que juste une fonction.

164
00:06:30,670 --> 00:06:34,179
Et donc, si maintenant je reprends tout ça,

165
00:06:34,279 --> 00:06:35,389
vous voyez que vous avez des termes

166
00:06:35,489 --> 00:06:36,605
qui sont des ressources,

167
00:06:36,705 --> 00:06:38,030
vous avez des opérations,

168
00:06:38,390 --> 00:06:39,944
vous avez des proportions,

169
00:06:40,044 --> 00:06:41,345
vous avez une notion de paramètre,

170
00:06:41,737 --> 00:06:43,300
tout ça, c'est des termes

171
00:06:43,400 --> 00:06:45,132
que l'on va retrouver dans les algorithmes.

172
00:06:45,500 --> 00:06:47,429
Mais la chose la plus importante,

173
00:06:47,529 --> 00:06:50,505
c'est ici, vous avez effectivement une recette de cuisine,

174
00:06:50,605 --> 00:06:51,592
mais on peut se poser la question

175
00:06:52,746 --> 00:06:54,168
pourquoi une recette de cuisine ?

176
00:06:55,658 --> 00:06:58,220
Pourquoi il n'a pas directement écrit un programme

177
00:06:58,320 --> 00:06:59,471
qui permette de faire des madeleines

178
00:06:59,571 --> 00:07:02,814
et vous appuyez sur un bouton.

179
00:07:03,186 --> 00:07:04,684
Pour ne pas citer de marque,

180
00:07:04,784 --> 00:07:06,421
vous avez des marques actuellement

181
00:07:06,521 --> 00:07:08,484
où vous mettez en vrac tous vos ingrédients

182
00:07:08,584 --> 00:07:09,271
dans la gamelle,

183
00:07:09,371 --> 00:07:12,513
vous regardez sur une recette

184
00:07:12,613 --> 00:07:13,799
et sur un petit écran tactile

185
00:07:13,899 --> 00:07:15,150
et puis vous appuyez sur un bouton

186
00:07:15,250 --> 00:07:18,031
et ça vous fait une soupe aux légumes,

187
00:07:18,224 --> 00:07:19,579
parfaitement cuite,

188
00:07:19,679 --> 00:07:20,639
et cætera.

189
00:07:21,843 --> 00:07:24,579
Ça, c'est effectivement,

190
00:07:24,679 --> 00:07:27,139
enfin, c'est une façon de faire la cuisine,

191
00:07:27,569 --> 00:07:29,931
mais en aucun cas, vous n'avez compris pourquoi

192
00:07:30,311 --> 00:07:32,340
il fallait faire les choses dans cet ordre-là,

193
00:07:32,440 --> 00:07:33,786
comme ça, comme ça, comme ça.

194
00:07:33,886 --> 00:07:36,182
Vous n'avez aucune information supplémentaire.

195
00:07:36,486 --> 00:07:38,917
Ici, vous avez des choses qui sont plus détaillées

196
00:07:39,017 --> 00:07:41,103
et on suppose que vous connaissez l'effet

197
00:07:41,203 --> 00:07:42,306
des différentes opérations,

198
00:07:43,211 --> 00:07:44,255
l'effet du tamisage,

199
00:07:44,355 --> 00:07:46,378
l'effet de faire mousser

200
00:07:46,710 --> 00:07:48,340
pour arriver à une certaine émulsion

201
00:07:48,440 --> 00:07:50,634
pour pouvoir avoir quelque chose de stable

202
00:07:50,734 --> 00:07:52,523
pour faire l'incorporation de la farine,

203
00:07:52,925 --> 00:07:54,220
et cætera, et cætera.

204
00:07:54,500 --> 00:07:56,605
Donc vous avez des informations supplémentaires

205
00:07:57,117 --> 00:07:58,834
qui vous sont données dans la recette.

206
00:07:59,712 --> 00:08:01,725
Maintenant, si je vais encore un peu plus loin,

207
00:08:02,181 --> 00:08:03,564
à quelle occasion

208
00:08:03,664 --> 00:08:05,029
écrit-on des recettes de cuisine ?

209
00:08:05,129 --> 00:08:07,848
En fait, on écrit des recettes de cuisine

210
00:08:07,948 --> 00:08:10,263
parce que on a envie de transmettre

211
00:08:10,363 --> 00:08:12,461
sa recette à quelqu'un d'autre.

212
00:08:12,814 --> 00:08:14,900
C'est-à-dire que la recette de cuisine

213
00:08:15,000 --> 00:08:16,168
est un médium,

214
00:08:16,685 --> 00:08:19,030
un objet que l'on se communique,

215
00:08:19,130 --> 00:08:22,967
alors de grand-mères en petits-enfants,

216
00:08:23,067 --> 00:08:26,325
pour essayer de garder vivante

217
00:08:26,425 --> 00:08:28,149
une recette de baba au rhum,

218
00:08:28,249 --> 00:08:29,787
de madeleines ou d'autre chose.

219
00:08:30,222 --> 00:08:32,234
Donc en fait, c'est un objet de communication.

220
00:08:33,425 --> 00:08:34,915
C'est-à-dire que la recette de cuisine,

221
00:08:35,015 --> 00:08:36,406
c'est un objet de communication,

222
00:08:36,895 --> 00:08:39,236
ce n'est pas seulement une méthode,

223
00:08:39,622 --> 00:08:42,132
c'est une méthode avec l'expression de cette méthode

224
00:08:42,232 --> 00:08:43,197
sous une certaine forme

225
00:08:43,297 --> 00:08:45,003
de façon à ce qu'elle soit compréhensible

226
00:08:45,103 --> 00:08:46,033
par quelqu'un d'autre.

227
00:08:46,800 --> 00:08:47,936
Donc si maintenant,

228
00:08:49,121 --> 00:08:50,358
je regarde un peu plus loin,

229
00:08:51,421 --> 00:08:54,088
informellement, c'est la description d'un processus,

230
00:08:54,188 --> 00:08:55,773
donc ce que je vous ai donné,

231
00:08:55,873 --> 00:08:58,221
et j'ai un objectif qui est très clair,

232
00:08:58,321 --> 00:09:00,336
des données, et des ressources qui sont données.

233
00:09:00,436 --> 00:09:02,852
Et on vient de voir un certain nombre de mots-clés,

234
00:09:03,268 --> 00:09:04,429
opérations,

235
00:09:05,201 --> 00:09:07,858
alors je n'ai volontairement pas mis le terme instructions

236
00:09:08,210 --> 00:09:10,406
parce que vous voyez qu'il y a un certain nombre d'opérations

237
00:09:10,506 --> 00:09:12,337
qui peuvent être décomposées

238
00:09:12,437 --> 00:09:13,574
en plusieurs instructions,

239
00:09:13,917 --> 00:09:15,665
donc je préfère parler d'opérations,

240
00:09:16,117 --> 00:09:17,455
des ressources,

241
00:09:17,827 --> 00:09:19,946
des tâches qui sont dépendantes les unes des autres,

242
00:09:20,046 --> 00:09:21,387
vous allez pouvoir faire

243
00:09:21,487 --> 00:09:24,265
uniquement le mélange farine, œufs

244
00:09:25,493 --> 00:09:27,673
et sucre que quand vous aurez battu en mousse,

245
00:09:27,773 --> 00:09:29,756
donc il y a des opérations qui se font dans un certain ordre,

246
00:09:30,106 --> 00:09:31,883
vous avez la notion de paramètre,

247
00:09:32,512 --> 00:09:34,387
vous avez la notion de garantie

248
00:09:34,487 --> 00:09:36,760
par rapport au résultat attendu,

249
00:09:36,860 --> 00:09:38,661
et vous avez la transmission

250
00:09:38,761 --> 00:09:40,231
puisque la recette, c'est un objet

251
00:09:40,331 --> 00:09:42,272
que l'on transmet à ses petits-enfants.

252
00:09:43,382 --> 00:09:44,484
Donc voilà,

253
00:09:44,584 --> 00:09:46,320
on commence à approcher maintenant

254
00:09:46,420 --> 00:09:48,167
une définition de l'algorithme

255
00:09:48,721 --> 00:09:53,073
un peu plus proche de la définition informatique.

256
00:09:53,456 --> 00:09:54,802
Donc un algorithme,

257
00:09:54,902 --> 00:09:57,317
c'est l'écriture d'une méthode

258
00:09:57,417 --> 00:09:59,678
conçue pour résoudre un ou des problèmes

259
00:10:00,105 --> 00:10:01,339
ou répondre à un objectif,

260
00:10:01,439 --> 00:10:03,880
implicitement de manière efficace,

261
00:10:03,980 --> 00:10:05,986
c'est-à-dire que on suppose que

262
00:10:06,479 --> 00:10:08,164
l'algorithme que vous proposez

263
00:10:08,264 --> 00:10:09,925
va être le bon algorithme,

264
00:10:10,141 --> 00:10:12,295
il va faire le bon résultat mais il va le faire

265
00:10:12,395 --> 00:10:13,894
et puis ça ne va pas vous prendre des plombes.

266
00:10:14,634 --> 00:10:16,809
Et donc, de manière efficace,

267
00:10:16,909 --> 00:10:18,364
et je peux répéter l'algorithme

268
00:10:18,464 --> 00:10:20,169
c'est-à-dire que c'est quelque chose de systématique

269
00:10:20,269 --> 00:10:21,043
qui est sous-jacent,

270
00:10:21,421 --> 00:10:22,977
et ça sous-entend

271
00:10:23,482 --> 00:10:25,076
un aspect machine

272
00:10:25,176 --> 00:10:26,856
c'est-à-dire quelque chose d'opérationnel,

273
00:10:27,865 --> 00:10:30,960
c'est-à-dire que derrière, je vais avoir la transformation du réel.

274
00:10:31,805 --> 00:10:34,375
Donc cet algorithme est destiné à être lu,

275
00:10:34,475 --> 00:10:35,724
compris, implémenté

276
00:10:35,824 --> 00:10:37,827
par une personne chargée de le rendre opérationnel,

277
00:10:38,230 --> 00:10:39,516
c'est-à-dire le programmeur

278
00:10:39,616 --> 00:10:40,612
en informatique.

279
00:10:41,193 --> 00:10:43,313
Et donc, il y a vraiment un lien très fort

280
00:10:43,413 --> 00:10:45,617
entre l'algorithme

281
00:10:45,717 --> 00:10:47,359
qui est conçu

282
00:10:47,459 --> 00:10:49,170
et le programme qui est exécuté.

283
00:10:49,543 --> 00:10:53,420
Et donc vous avez quatre entités,

284
00:10:54,205 --> 00:10:56,203
l'algorithmicien qui va définir,

285
00:10:56,303 --> 00:10:57,816
qui va construire un algorithme,

286
00:10:57,916 --> 00:10:59,498
le programmeur qui va

287
00:10:59,598 --> 00:11:01,004
implémenter cet algorithme,

288
00:11:01,104 --> 00:11:02,589
et puis vous avez l'algorithme et le programme,

289
00:11:03,039 --> 00:11:04,380
et il ne faut surtout pas croire

290
00:11:04,480 --> 00:11:06,831
que le programmeur ne discute pas avec l'algorithmicien

291
00:11:06,931 --> 00:11:09,178
puisque ce sont les opérateurs

292
00:11:09,278 --> 00:11:10,543
qui seront des opérations

293
00:11:10,643 --> 00:11:12,478
qui seront mises à disposition

294
00:11:12,578 --> 00:11:13,482
de l'algorithmicien

295
00:11:13,582 --> 00:11:15,050
par le programmeur

296
00:11:15,150 --> 00:11:17,239
qui vont lui permettre de faire son algorithme.

297
00:11:17,823 --> 00:11:19,565
Donc ça, c'est le premier point,

298
00:11:19,665 --> 00:11:20,569
le deuxième point,

299
00:11:21,061 --> 00:11:23,419
c'est que c'est un objet de communication.

300
00:11:23,863 --> 00:11:26,028
C'est-à-dire qu'il est destiné à être lu,

301
00:11:26,128 --> 00:11:27,776
destiné à être interprété

302
00:11:28,213 --> 00:11:30,929
et donc il y a une certaine systématisation

303
00:11:31,029 --> 00:11:32,797
dans le langage qui est utilisé,

304
00:11:32,897 --> 00:11:34,411
donc il y a un langage de référence.

305
00:11:34,511 --> 00:11:35,995
Dans de nombreux ouvrages,

306
00:11:36,095 --> 00:11:39,073
les gens essaient de définir un pseudo langage algorithmique

307
00:11:39,173 --> 00:11:41,999
et nous verrons dans une des vidéos suivantes

308
00:11:42,399 --> 00:11:45,141
que ce n'est pas forcément une bonne idée directement.

309
00:11:46,075 --> 00:11:49,349
On suppose que l'on a un niveau d'abstraction homogène.

310
00:11:49,828 --> 00:11:52,135
Par exemple, dans la recette de Pierre Hermé,

311
00:11:52,445 --> 00:11:54,828
il ne va pas vous expliquer comment casser des œufs.

312
00:11:56,587 --> 00:11:58,064
Il vous dit "cassez des oeufs",

313
00:11:58,164 --> 00:11:59,719
ça sous-entend que vous savez faire,

314
00:11:59,819 --> 00:12:01,250
"tamisez la farine",

315
00:12:01,350 --> 00:12:02,805
ça sous-entend que vous savez faire.

316
00:12:03,119 --> 00:12:04,917
Et vous avez un certain niveau d'abstraction.

317
00:12:05,267 --> 00:12:07,092
Et puis vous avez un contexte

318
00:12:07,537 --> 00:12:10,062
qui vous permet d'analyser ce que vous allez faire,

319
00:12:10,162 --> 00:12:11,566
donc de comprendre ce qu'il se passe,

320
00:12:12,021 --> 00:12:13,663
d'analyser les différentes étapes,

321
00:12:14,000 --> 00:12:16,982
de dire effectivement comment ça va se comporter,

322
00:12:17,267 --> 00:12:18,784
vous pouvez à peu près voir,

323
00:12:18,884 --> 00:12:20,587
vous pouvez faire en gros la preuve

324
00:12:20,687 --> 00:12:22,149
et l'analyse de complexité,

325
00:12:22,249 --> 00:12:23,920
quand on parlera d'algorithme informatique.

326
00:12:24,293 --> 00:12:25,972
Et, c'est le plus important,

327
00:12:26,072 --> 00:12:28,398
c'est o-pé-ra-tion-nel.

328
00:12:28,944 --> 00:12:32,844
C'est ça qui est le point clé,

329
00:12:32,944 --> 00:12:35,276
c'est-à-dire que la rédaction de votre algorithme

330
00:12:35,376 --> 00:12:36,681
doit permettre à quelqu'un

331
00:12:36,781 --> 00:12:37,409
d'implémenter

332
00:12:37,837 --> 00:12:39,333
et d'exécuter cet algorithme

333
00:12:39,433 --> 00:12:41,200
pour résoudre le problème correspondant.

334
00:12:42,203 --> 00:12:44,155
Donc ça, ça suppose évidemment

335
00:12:44,255 --> 00:12:46,442
que vous avez les machines bien spécifiées

336
00:12:46,542 --> 00:12:47,852
qui sont sous-jacentes.

