## Escape game "Le château" 3/4

1. [Escape game "Le château" 1/4](2-1-2-1_Escape_game-1.md)  
2. [Escape game "Le château" 2/4](2-1-2-2_Escape_game-2.md)
3. **Escape game "Le château" 3/4** 
4. [Escape game "Le château" 4/4](2-1-2-4_Escape_game-4.md)


[![Vidéo 3 B2-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M1-S2-video3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M1-S2-video3.mp4)
