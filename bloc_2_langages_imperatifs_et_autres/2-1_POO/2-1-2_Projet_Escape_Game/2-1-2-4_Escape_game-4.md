## Escape game "Le château" 4/4

1. [Escape game "Le château" 1/4](2-1-2-1_Escape_game-1.md)  
2. [Escape game "Le château" 2/4](2-1-2-2_Escape_game-2.md)
3. [Escape game "Le château" 3/4](2-1-2-3_Escape_game-3.md)
4. **Escape game "Le château" 4/4** 

[![Vidéo 4 B2-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M1-S2-video4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M1-S2-video4.mp4)
