# Projet Château

Initialement proposé par J. Olgiati et T. Massart dans le cadre du MOOC _Apprendre à Coder avec Python_, ce projet de jeu type _escape game_ va nous permettre d'aborder divers aspects de la programmation orienté objet, en une application de taille raisonnable. Nous verrons la conception et l'implémentation orientée objet. L'interface graphique sera rudimentaire et utilisera le module Turtle.

## Le jeu

Enfermé dans un château, représentant un labyrinthe de succession de pièces, un héros (vous) doit se frayer un chemin vers la sortie. Des portes verrouillées vous bloqueront souvent le passage et vous ne pourrez les ouvrir qu'en répondant correctement à des questions.

Heureusement, à divers endroits dans le château, vous pourrez ramasser des indices, qui aident à répondre aux énigmes.

La mécanique est simple : le jeu se passe dans une fenêtre graphique _Turtle_, le héros est matérialisé par une petite pastille rouge que vous dirigez via les flèches du clavier. Les questions sont déclenchées lorsqu'on passe à certains endroits (repérés en orange sur l'animation ci-dessous) et affichées par un système de _popup_ du module Turtle.

![jeu du chateau](2-1-2-5_projet_chateau/video_jeu.gif)

[![Vidéo jeu du chateau ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M1-jeu-chateau.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M1-jeu-chateau.mp4)

## La suite

### Analyse

Une première analyse est réalisée dans les quatre prochaines vidéos et peut servir de point de départ pour votre travail.

### À vous de jouer

Il s'agira pour vous de réaliser entièrement le projet, en utilisant la POO. Libre à vous d'améliorer le projet initial :

- le plan du château pourrait ne se dévoiler qu'au fur et à mesure de l'avancée ;
- un timer pourrait être ajouté ;
- des pièges ;
- etc.
