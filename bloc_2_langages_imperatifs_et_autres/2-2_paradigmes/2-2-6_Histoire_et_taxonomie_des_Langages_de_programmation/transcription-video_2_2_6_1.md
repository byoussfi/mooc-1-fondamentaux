# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 2.2.6.1 : Premiers langages de programmation

Pour rédiger cette partie qui résumé brièvement l'histoire et quelques paradigmes des langages de programmation, nous nous sommes basé sur les supports donnés en fin de cette vidéo ou dans le support de notes

La genèse des tout premiers langages de programmation avec FORTRAN, COBOL, ALGOL et LISP par exemple a montré que leur évolution allait être très rapide :

les versions, voire générations différentes se succédaient rapidement.

De plus, les dialectes se multipliant (au gré du développement de compilateurs, bibliothèques et runtimes associés)

Mais ce qui est remarquable également, c’est le foisonnement de nouveaux langages qui sont apparus rapidement, presque toujours comme héritier manifeste d’un ou plusieurs prédécesseurs, que cette filiation soit annoncée et acceptée ou non.

Celle-ci explique la grande similitude entre langages.  C'est aussi une grande chance pour les candidats-programmeurs lors de l’étude d’un nouveau langage.

Il existe de nombreux auteurs qui proposent une généalogie des différents langages usuels.

Au-delà de cette visualisation historique des créations de langages, il est utile d’essayer de les regrouper par familles présentant un grand ensemble de caractéristiques communes.

Par exemple, FORTRAN, défini en 1956-57, visait à réaliser des calculs scientifiques.

Sa syntaxe est très proche de l’écriture algébrique usuelle.

Par contre, les structures de programmation originelles sont quasi absentes : une boucle à compteur et des sauts conditionnels, mais aucune instruction composée algorithmique de type « while » ou « if then else ».

Par contre, COBOL, défini en 1959–60, est essentiellement conçu pour faire des entrées-sorties.

Ces instructions y sont riches et nombreuses, ainsi que les capacités de décrire et manipuler des fichiers.

Par contre, il ne comporte toujours quasi aucune instruction de calcul proprement dit.

Et ALGOL, défini entre 1958 et 1960, a été imaginé par des algorithmiciens.

Sa syntaxe est celle d’une programmation structurée mature, mais il n’a aucune instruction standard d’entrées-sorties !

Pour cela, il a fallu attendre ALGOL68 — un autre langage, mais un dérivé très proche d’ALGOL — qui est vraiment l’archétype presque parfait d’un langage algorithmique.

Quant à LISP , défini en 1958, il ne devait originellement servir qu’à décrire mathématiquement des programmes sous forme de formules logiques proches du λ-calcul.

Bien sûr, il n’existe pas une seule façon de mettre de l’ordre dans cet univers, et la répartition que nous présentons ici est largement arbitraire... et bien sûr incomplète.

De plus, les frontières ainsi introduites sont floues et poreuses.


-----------

Références

[Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. I (3ème édition) 2020](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/e68c3b0dfd5351796c8dfa72b1125ec866174fee/bloc_2_langages_imperatifs_et_autres/R%C3%A9f%C3%A9rences/Langages_I.pdf)

[Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. II (4ème édition) 2020](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/e68c3b0dfd5351796c8dfa72b1125ec866174fee/bloc_2_langages_imperatifs_et_autres/R%C3%A9f%C3%A9rences/Langages_II.pdf)

https://fr.wikipedia.org/wiki/Paradigme_(programmation)

http://projet.eu.org/pedago/sin/ICN/1ere/4-langages.pdf



