## Autres langages (programmation concurrente, événementielle, ...)

Bien sûr, sans parler des langages exotiques, il existe de nombreux autres langages qui n’entrent pas parfaitement dans l’une des catégories « impératif »,  « orienté-objet » ou « déclaratif », et qui pourtant sont Turing complets et permettent parfaitement de « programmer ».

L'entrée (Paradigme de programmation, section Autres_types  de wikipedia](https://fr.wikipedia.org/wiki/Paradigme_(programmation)#Autres_types) donne des dizaines de paradigmes différents.

Citons en particulier

- la programmation événementielle où un programme sera principalement défini par ses réactions aux différents événements qui peuvent se produire (Par exemple, TcL/Tk gère un tel paradigme) ;
- la programmation concurrente où plusieurs processus (appelés également threads, ou tâches) s'exécutent en concurrence, donnant lieu à de nombreux problèmes informatiques complexes dont les célèbres problèmes d'[interblocage](https://fr.wikipedia.org/wiki/Interblocage) et de [famines](https://fr.wikipedia.org/wiki/Famine_(informatique))).



De nombreux **langages dédiés** (**domain-specific languages** ou **DSL**) sont conçus pour répondre aux contraintes d’un domaine d'application précis. On peut citer, comme simples exemples parmi beaucoup d’autres :

- Les langages de requêtes (query languages) : ils sont a priori conçus pour interroger une base de données ou un système d’information. SQL en est l’archétype.
- Les langages de script (scripting languages) : à l’origine, il s’agissait des langages associés à un système d’exploitation (un JCL, Job Control Language). De nombreux autres langages interprétés sont venus enrichir cette catégorie : Bourne shell (sh) et ses variantes (csh, ksh, bash, tcsh, zsh...), Perl, Raku, Ruby, Lua, Tcl/Tk...
- Les langages d’analyse statistique (statistical computing languages) : ils offrent des outils de traitement, d’analyse et de présentation graphique de grands ensembles de données. Exemples : R, SAS, SPSS, Statistica...
- Les langages de calcul numérique (numerical computing languages) : d’un certain point de vue, ils sont une généralisation des précédents ; ce sont des outils de calculs complexes sur des données générales et de présentation graphique des résultats obtenus. Exemples : Gnu Octave, MATLAB, Mathematica, Scilab... Le langage APL (1966) était un précurseur de cette famille.
- Les langages de calcul formel (computer algebra languages) : ces langages manipulent des valeurs qui sont des formules mathématiques, pas des valeurs numériques. Des exemples très connus sont Maple, Maxima, Reduce, Derive, MATHLAB (ne pas confondre avec l’autre)...
- etc.

Pour illustrer ce qu’est un calcul formel, voici un exemple en Maxima. Si l’on écrit :

```maxima
factor(x^2-3*x+2);
```

il répond (i.e. il évalue) : « (x-2)(x-1) ». Si l’on veut la dérivée première :

```maxima
diff(x^2-3*x+2,x,1);
```

il répond : «2x-3».

### Langages multi-paradigmes

Souvent les langages sont multi-paradigmes : ainsi Python est un langage impératif, orienté-objet et permet une certaine programmation fonctionnelle. Si l'on regarde l'ébauche d'article [Comparaison_des_langages_de_programmation_multi-paradigmes sur wikipedia](https://fr.wikipedia.org/wiki/Comparaison_des_langages_de_programmation_multi-paradigmes), 10 paradigmes sont cités pour Python (le langage [Julia](https://fr.wikipedia.org/wiki/Julia_(langage)) en a plus de 17 !).
