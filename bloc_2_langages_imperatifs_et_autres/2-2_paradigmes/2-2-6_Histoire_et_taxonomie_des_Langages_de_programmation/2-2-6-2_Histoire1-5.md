<!-- # Histoire et taxonomie des langages de programmation -->

> Pour rédiger cette partie, nous nous sommes basé sur les supports :
>
> [Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. I (3ème édition) 2020](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/e68c3b0dfd5351796c8dfa72b1125ec866174fee/bloc_2_langages_imperatifs_et_autres/R%C3%A9f%C3%A9rences/Langages_I.pdf)
>
> [Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. II (4ème édition) 2020](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/e68c3b0dfd5351796c8dfa72b1125ec866174fee/bloc_2_langages_imperatifs_et_autres/R%C3%A9f%C3%A9rences/Langages_II.pdf)

> voir aussi
> https://fr.wikipedia.org/wiki/Paradigme_(programmation)
> http://projet.eu.org/pedago/sin/ICN/1ere/4-langages.pdf

Nous invitons le lecteur à utiliser ces références pour compléter l'information résumée ici.

## Premiers langages de programmation

La genèse des tout premiers langages (FORTRAN, COBOL, ALGOL, LISP...) a montré que leur évolution allait être très rapide : les versions, voire générations différentes, se succédaient rapidement. De plus, les dialectes se multipliant (au gré du développement de compilateurs, bibliothèques et runtimes associés), il était indispensable de fixer leur définition, que ce soit par des normes officielles ou des standards de fait.

Mais ce qui est remarquable également, c'est le foisonnement de nouveaux langages qui sont apparus rapidement, presque toujours comme héritiers manifestes d'un ou plusieurs prédécesseurs, que cette filiation soit annoncée et acceptée ou non. Ceci explique la grande similitude entre langages, pour autant qu'on ne s'attarde pas trop sur de petits détails, bien sûr, mais c'est une grande chance pour les candidats-programmeurs lors de l'étude d'un nouveau langage.

Il existe de nombreux auteurs qui proposent une généalogie des différents langages usuels ; elles sont heureusement très proches.

> On peut citer [Éric Lévénez](www.levenez.com/lang) (qui se limite à une cinquantaine de langages)  ou la [HOPL (History Of Programming Languages) de DiarmuidPigott](hopl.info). Voir aussi la [liste des langages de RosettaCode.org](rosettacode.org/wiki/Category:Programming_Languages).

Mais au-delà de cette visualisation historique des créations de langages, il est utile d'essayer de les regrouper par familles présentant un grand ensemble de caractéristiques communes. Celles-ci sont d'ailleurs fortement liées aux objectifs, à l'usage envisagé pour le langage par ses concepteurs.

Par exemple, FORTRAN (1956–57) visait à réaliser des calculs scientifiques. Sa syntaxe est très proche de l'écriture algébrique usuelle et sa bibliothèque de fonctions transcendantes (logarithmes, trigonométriques, etc.) est très riche, dès le début. Par contre, les structures de programmation originelles sont quasi absentes : une boucle à compteur et des sauts conditionnels, mais aucune instruction composée algorithmique de type « while » ou « if then else ».

Par contre, COBOL (1959–60) est essentiellement conçu pour faire des entrées-sorties. Ces instructions y sont riches et nombreuses, ainsi que les capacités de décrire et manipuler des fichiers. Par contre, il ne comporte toujours quasi aucune instruction de calcul proprement dit.

Et ALGOL (1958–60) a été imaginé par des algorithmiciens. Sa syntaxe est celle d'une programmation structurée mature, mais il n'a aucune instruction standard d'entrées-sorties ! Pour cela, il a fallu attendre ALGOL68 — un autre langage, mais un dérivé très proche d'ALGOL — qui est vraiment l'archétype presque parfait d'un langage algorithmique. Sa syntaxe ou des variantes très proches servent d'ailleurs généralement pour la description des algorithmes dans les ouvrages et publications scientifiques de ce domaine.

Quant à LISP (1958), il ne devait originellement servir qu'à décrire mathématiquement des programmes sous forme de formules logiques proches du [λ-calcul](https://fr.wikipedia.org/wiki/Lambda-calcul). Un programme dans ce langage peut donc, par essence, décrire et produire (i.e. calculer) un autre programme, le programme lui-même étant une donnée de lui-même et tout code étant potentiellement automodifiant. Il n'est toujours pas standardisé comme tel et il existe donc une foison de dialectes différents et incompatibles, dont certains sont finalement devenus des normes (Common Lisp en 1994, par exemple). Ce langage reste très utilisé dans le domaine de l'intelligence artificielle.
Et il en est de même de tous les autres.
Bien sûr, il n'existe pas une seule façon de mettre de l'ordre dans cet univers, et la répartition que nous présentons ici est largement arbitraire... et bien sûr incomplète. De plus, les frontières ainsi introduites sont floues et poreuses.

> Cette description des paradigmes est librement inspirée de l'ouvrage de vulgarisation (Torra I. Reventós, Vicenç, Del Ábaco A La Revolución Digital: Algoritmos y Computación, RBA Libros, 2011, 150p.) (traduit dans de nombreuses langues).

