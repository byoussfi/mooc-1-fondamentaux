1
00:00:00,000 --> 00:00:02,164
Notons d'abord que si l'on peut calculer

2
00:00:02,264 --> 00:00:04,255
qu'un algorithme est en O de n carré,

3
00:00:04,456 --> 00:00:07,284
il n'est pas intéressant, même si c'est trivialement vrai,

4
00:00:07,384 --> 00:00:08,856
d'exprimer le fait que

5
00:00:08,956 --> 00:00:11,525
l'algorithme est en O de n cube,

6
00:00:11,625 --> 00:00:13,092
O de n^4, et cætera.

7
00:00:13,192 --> 00:00:14,784
Lorsque l'on évalue

8
00:00:14,884 --> 00:00:16,379
le O d'une complexité,

9
00:00:16,479 --> 00:00:18,421
c'est la meilleure estimation simple

10
00:00:18,521 --> 00:00:19,547
que l'on cherche à obtenir.

11
00:00:20,347 --> 00:00:23,302
Ainsi, on classe généralement les complexités d'algorithmes

12
00:00:23,402 --> 00:00:27,002
selon leur O, comme donnés par la table suivante.

13
00:00:27,102 --> 00:00:29,618
D'abord, les codes qui prennent un temps borné

14
00:00:29,718 --> 00:00:31,113
sont en O de 1,

15
00:00:31,213 --> 00:00:32,444
complexité constante.

16
00:00:32,544 --> 00:00:35,494
Ensuite les algorithmes de complexité logarithmique

17
00:00:35,594 --> 00:00:36,810
en O de log n.

18
00:00:36,910 --> 00:00:39,122
Ensuite, linéaire en O de n.

19
00:00:39,720 --> 00:00:41,679
Ensuite, O de n log n.

20
00:00:42,522 --> 00:00:45,184
Ensuite, les algorithmes à complexité polynomiale

21
00:00:45,284 --> 00:00:46,804
O de n carré, quadratique,

22
00:00:46,904 --> 00:00:48,986
O de n cube, cubique, et cætera.

23
00:00:49,418 --> 00:00:51,336
Ensuite les complexités exponentielles,

24
00:00:51,436 --> 00:00:52,846
O de 2 exposant n,

25
00:00:52,946 --> 00:00:54,857
O de 3 exposant n, et caetera,

26
00:00:54,957 --> 00:00:58,250
O de n exposant n, c'est-à-dire exponentiel en base n,

27
00:00:58,350 --> 00:00:59,283
et ainsi de suite.

28
00:00:59,798 --> 00:01:01,938
Remarquons qu'en vertu du fait que,

29
00:01:02,038 --> 00:01:03,869
pour tous a, b positifs,

30
00:01:03,969 --> 00:01:05,230
log a de n

31
00:01:05,330 --> 00:01:09,079
est égal à log a de b fois log b de n,

32
00:01:09,179 --> 00:01:13,411
comme log a de b est une constante pour a et b fixés,

33
00:01:13,511 --> 00:01:17,064
si T(n) est en O de log a de n,

34
00:01:17,164 --> 00:01:19,751
il est également en O de log b de n.

35
00:01:20,176 --> 00:01:22,290
La base du logarithme n'a donc pas d'importance

36
00:01:22,390 --> 00:01:24,299
pour exprimer un O.

37
00:01:24,399 --> 00:01:27,124
Notons encore que ceci n'est pas vrai

38
00:01:27,224 --> 00:01:30,595
pour les bases des complexités exponentielles.

39
00:01:31,090 --> 00:01:33,493
Donnons ici un petit nombre de règles

40
00:01:33,593 --> 00:01:36,084
permettant d'évaluer la complexité d'un algorithme.

41
00:01:36,467 --> 00:01:38,041
Ces règles ne donnent en général

42
00:01:38,141 --> 00:01:40,873
qu'une sur-approximation parfois grossière,

43
00:01:40,973 --> 00:01:42,490
une estimation plus précise

44
00:01:42,590 --> 00:01:45,426
devant tenir compte du fonctionnement de l'algorithme.

45
00:01:46,095 --> 00:01:49,952
Tout d'abord, l'évaluation se fait en bottom-up,

46
00:01:50,052 --> 00:01:51,932
depuis les instructions simples

47
00:01:52,032 --> 00:01:53,869
vers les instructions plus complexes.

48
00:01:54,129 --> 00:01:56,080
Si l'on regarde l'arbre syntaxique

49
00:01:56,180 --> 00:01:57,925
du code de la recherche du minimum,

50
00:01:58,025 --> 00:01:59,629
on détermine d'abord

51
00:02:00,626 --> 00:02:02,241
la complexité des feuilles

52
00:02:02,341 --> 00:02:05,641
avec les instructions 1, 2, 5, 6 et 7,

53
00:02:05,741 --> 00:02:06,971
pour ensuite remonter

54
00:02:07,071 --> 00:02:09,825
en déterminant la complexité de l'instruction if

55
00:02:10,058 --> 00:02:11,817
et ensuite du while

56
00:02:11,917 --> 00:02:14,170
pour enfin avoir la complexité de tout le code

57
00:02:14,270 --> 00:02:16,552
qui correspond à la racine de l'arbre syntaxique.

58
00:02:19,876 --> 00:02:22,721
Règle 1 ou règle de l'unité :

59
00:02:23,403 --> 00:02:26,132
Il faut d'abord clairement définir l'unité utilisée

60
00:02:26,232 --> 00:02:27,478
et les éléments pris en compte.

61
00:02:27,578 --> 00:02:28,141
Ainsi,

62
00:02:28,241 --> 00:02:30,795
si l'on regarde la complexité en temps d'exécution,

63
00:02:30,895 --> 00:02:32,302
et si l'on tient compte

64
00:02:32,402 --> 00:02:33,587
des tests élémentaires,

65
00:02:33,687 --> 00:02:34,561
des assignations,

66
00:02:34,661 --> 00:02:36,099
des lectures de données,

67
00:02:36,199 --> 00:02:37,324
des écritures de résultat,

68
00:02:37,424 --> 00:02:39,961
une assignation à une variable simple,

69
00:02:40,061 --> 00:02:41,090
une instruction input

70
00:02:41,190 --> 00:02:42,963
ou print d'une donnée simple,

71
00:02:43,063 --> 00:02:44,640
ou l'évaluation d'une expression

72
00:02:44,740 --> 00:02:45,786
ou d'une condition simple

73
00:02:45,886 --> 00:02:48,369
ne donnant pas lieu à l'exécution de fonction

74
00:02:48,469 --> 00:02:50,564
prend un temps constant fixé,

75
00:02:50,664 --> 00:02:53,050
est donc en O(1).

76
00:02:53,150 --> 00:02:56,113
L'assignation à des objets plus complexes

77
00:02:56,213 --> 00:02:58,662
ou les input ou print de données plus complexes

78
00:02:58,762 --> 00:03:00,144
peut ne pas être en O(1),

79
00:03:00,244 --> 00:03:01,620
en fonction de leur taille

80
00:03:01,720 --> 00:03:04,358
qui peut dépendre de la taille du problème n.

81
00:03:04,458 --> 00:03:05,315
Par exemple,

82
00:03:05,415 --> 00:03:08,190
en comptabilisant le temps de toutes les instructions,

83
00:03:09,189 --> 00:03:11,356
chacune des instructions suivantes

84
00:03:11,456 --> 00:03:13,320
est en O(1).

85
00:03:15,554 --> 00:03:16,404
La règle 2

86
00:03:16,504 --> 00:03:18,002
qui est la règle de la séquence

87
00:03:18,102 --> 00:03:18,856
dit la chose suivante.

88
00:03:18,956 --> 00:03:22,564
Si un traitement 1 prend un temps T1(n)

89
00:03:22,664 --> 00:03:24,389
qui est en O(f1(n)),

90
00:03:24,489 --> 00:03:27,041
et un traitement 2 prend un temps T2(n)

91
00:03:27,141 --> 00:03:28,636
qui est en O(f2(n)),

92
00:03:28,864 --> 00:03:31,845
alors le traitement 1 suivi du traitement 2

93
00:03:31,945 --> 00:03:35,633
prend un temps T1(n) + T2(n)

94
00:03:35,733 --> 00:03:38,363
et est en O(f1(n) + f2(n))

95
00:03:38,463 --> 00:03:43,587
qui est égal au O du maximum de f1(n) et de f2(n),

96
00:03:44,152 --> 00:03:46,515
où max prend la fonction qui croît le plus vite,

97
00:03:46,615 --> 00:03:47,955
c'est-à-dire de plus grand ordre.

98
00:03:48,448 --> 00:03:51,705
Par exemple, si T1(n) est en O(n carré),

99
00:03:51,805 --> 00:03:54,105
et T2(n) est en O(n),

100
00:03:54,205 --> 00:03:58,149
T1(n) + T2(n) est en O(n carré + n),

101
00:03:58,249 --> 00:04:01,143
qui est égal à O(n carré).

102
00:04:01,971 --> 00:04:04,722
En particulier, une séquence d'instructions simples

103
00:04:04,822 --> 00:04:06,959
dont on tient compte

104
00:04:07,059 --> 00:04:09,309
ne faisant aucun appel à une procédure

105
00:04:09,409 --> 00:04:10,249
ou à une fonction,

106
00:04:10,349 --> 00:04:11,741
ne dépend pas de n

107
00:04:11,841 --> 00:04:13,736
et est donc en O(1).

108
00:04:13,836 --> 00:04:16,936
Par exemple, la séquence de toutes les instructions simples précédentes

109
00:04:17,036 --> 00:04:18,693
est en O(1).

110
00:04:19,557 --> 00:04:20,551
Règle 3 :

111
00:04:21,037 --> 00:04:22,292
règle du if.

112
00:04:25,896 --> 00:04:30,095
Pour un if condition instruction_1 else instruction_2,

113
00:04:30,742 --> 00:04:34,155
où instruction_1 est en O(f1(n))

114
00:04:34,255 --> 00:04:36,738
et instruction_2 est en O(f2(n)),

115
00:04:36,932 --> 00:04:40,676
l'évaluation de la condition est en O(g(n)),

116
00:04:41,415 --> 00:04:43,383
suivant le test, le if sera

117
00:04:43,483 --> 00:04:46,372
soit en O(max(f1(n), g(n))),

118
00:04:46,472 --> 00:04:48,880
soit en O(max(f2(n), g(n))

119
00:04:48,980 --> 00:04:53,638
et peut être borné par O(max(f1(n), f2(n), g(n)).

120
00:04:54,112 --> 00:04:57,530
Notons que souvent g(n) est en O(1).

121
00:04:57,630 --> 00:04:59,195
Par exemple, le code suivant

122
00:04:59,295 --> 00:05:01,289
est en O(1).

123
00:05:03,655 --> 00:05:05,706
Cette règle peut être généralisée

124
00:05:06,112 --> 00:05:07,995
pour une instruction if ayant

125
00:05:08,095 --> 00:05:10,556
une ou plusieurs parties elif évidemment.

126
00:05:12,507 --> 00:05:13,362
Règle 4 :

127
00:05:13,462 --> 00:05:14,526
règle du while.

128
00:05:15,400 --> 00:05:17,072
Pour un while sachant que

129
00:05:17,172 --> 00:05:19,892
le corps de la boucle est en O(f1(n))

130
00:05:19,992 --> 00:05:23,320
et que l'évaluation de la condition est en O(f2(n)),

131
00:05:23,420 --> 00:05:28,153
sachant aussi que l'on a une fonction O(g(n))

132
00:05:28,253 --> 00:05:30,826
qui donne une borne supérieure

133
00:05:30,926 --> 00:05:33,079
du nombre de fois que le corps sera exécuté,

134
00:05:33,700 --> 00:05:37,754
alors le while est en O de f(n) fois g(n),

135
00:05:37,854 --> 00:05:42,091
où f(n) est le maximum de f1(n) et de f2(n).

136
00:05:42,990 --> 00:05:44,133
Par exemple,

137
00:05:44,233 --> 00:05:46,504
la fonction de recherche dans une liste s

138
00:05:46,704 --> 00:05:48,997
de l'indice i de l'élément de s

139
00:05:49,097 --> 00:05:52,273
dont la composante s[i][0] vaut x

140
00:05:52,373 --> 00:05:53,771
est en O(n),

141
00:05:53,871 --> 00:05:55,110
où n est la taille de s.

142
00:05:55,210 --> 00:05:57,298
On suppose que les éléments de s

143
00:05:57,398 --> 00:05:58,744
sont bien sûr des tuples.

144
00:06:00,133 --> 00:06:01,157
Règle 5 :

145
00:06:01,717 --> 00:06:03,099
règle du for.

146
00:06:03,199 --> 00:06:04,199
Pour une boucle for,

147
00:06:04,299 --> 00:06:05,915
en fait il suffit, grosso modo,

148
00:06:06,015 --> 00:06:07,423
de traduire le for en while

149
00:06:07,523 --> 00:06:09,933
et donc, par exemple,

150
00:06:10,554 --> 00:06:13,837
si on a : for i in range(100)

151
00:06:13,937 --> 00:06:15,577
print(i)

152
00:06:15,677 --> 00:06:17,561
on voit ici que

153
00:06:17,661 --> 00:06:22,724
la fonction du nombre d'itérations est en O(1)

154
00:06:22,824 --> 00:06:24,781
puisque c'est 100 fois, qui est une constante,

155
00:06:25,337 --> 00:06:28,292
et on a le print qui est en O(1),

156
00:06:28,392 --> 00:06:31,485
et le for complet est en O(1).

157
00:06:31,585 --> 00:06:33,911
Par contre, si j'ai une liste s,

158
00:06:34,011 --> 00:06:35,396
d'entiers par exemple,

159
00:06:35,496 --> 00:06:38,473
avec n éléments,

160
00:06:38,573 --> 00:06:41,547
si je fais : for elem in s : print(elem)

161
00:06:41,647 --> 00:06:44,958
et bien ici, j'aurai le nombre d'itérations

162
00:06:45,058 --> 00:06:47,258
qui correspondra à la taille de ma liste,

163
00:06:47,358 --> 00:06:49,572
c'est-à-dire qui sera en O(n)

164
00:06:49,672 --> 00:06:52,123
et donc le for complet est en O(n).

165
00:06:53,515 --> 00:06:54,961
La règle 6

166
00:06:55,100 --> 00:06:57,037
est la règle sur les fonctions

167
00:06:57,764 --> 00:06:58,815
et dit la chose suivante.

168
00:06:58,915 --> 00:07:00,246
L'appel à une fonction

169
00:07:00,346 --> 00:07:02,264
est en O(f(n))

170
00:07:02,364 --> 00:07:05,122
correspondant à la complexité du traitement de cette fonction

171
00:07:05,222 --> 00:07:07,122
pour les paramètres effectifs donnés.

172
00:07:07,398 --> 00:07:08,680
Donnons un exemple.

173
00:07:09,908 --> 00:07:13,220
Supposons qu'on ait la fonction suivante

174
00:07:13,320 --> 00:07:14,909
ecrit_carre_nxn

175
00:07:15,280 --> 00:07:17,475
qui, après analyse, 

176
00:07:17,575 --> 00:07:19,011
on se rend compte qu'elle est en n^2

177
00:07:19,111 --> 00:07:20,746
où n est la taille du paramètre.

178
00:07:22,290 --> 00:07:23,818
n ici, c'est un entier

179
00:07:23,918 --> 00:07:25,879
et donc on a du O(n carré).

180
00:07:26,427 --> 00:07:29,103
Si l'on appelle cette fonction avec une valeur

181
00:07:29,885 --> 00:07:32,115
de l'argument qui vaut taille au cube,

182
00:07:32,215 --> 00:07:33,946
où taille est par exemple un entier lu,

183
00:07:34,046 --> 00:07:35,648
la complexité résultante

184
00:07:35,748 --> 00:07:40,050
est effectivement en O de taille exposant 6

185
00:07:40,484 --> 00:07:43,459
soit le fait que l'algorithme est en O de n carré

186
00:07:43,559 --> 00:07:47,258
mais que le n vaille taille au cube ici.

187
00:07:49,600 --> 00:07:51,148
Alors de façon générale,

188
00:07:51,248 --> 00:07:52,792
on peut raffiner ces calculs

189
00:07:53,400 --> 00:07:56,375
selon que l'on essaie de trouver une complexité minimale,

190
00:07:56,675 --> 00:07:58,602
moyenne ou maximale,

191
00:07:59,000 --> 00:08:00,630
ce raffinement se situera

192
00:08:00,730 --> 00:08:03,037
dans le nombre de fois qu'une instruction sera répétée

193
00:08:03,137 --> 00:08:05,597
ayant par exemple certaines hypothèses

194
00:08:05,697 --> 00:08:08,243
sur la probabilité que certaines conditions du if

195
00:08:08,343 --> 00:08:09,343
et des boucles soient vérifiées, et cætera.

