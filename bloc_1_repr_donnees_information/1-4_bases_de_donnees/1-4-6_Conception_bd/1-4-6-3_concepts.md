## Conception d'une base de données 3/5 :  concepts avancés

> **Note**  
> Pour la formation NSI, cette partie est optionnelle, car elle n'est pas au programme. Laissée ici pour ceux et celles qui veulent en savoir plus mais pas d'évaluation sur ce sujet.

>  **Supports complémentaires**
>
> -   [Diapositives : concepts avancés de modélisation](http://sql.bdpedia.fr/files/slea-avance.pdf)  
> -   [Vidéo sur les concepts avancés de modélisation / association](https://mediaserver.cnam.fr/videos/ea-avance/)  

Cette session présente quelques extensions courantes aux principes de base du modèle entité-association.

### Entités faibles

Jusqu'à présent nous avons considéré le cas d'entités *indépendantes* les unes des autres. Chaque entité, disposant de son propre identifiant, pouvait être considérée isolément. Il existe des cas où une entité ne peut exister qu'en étroite association avec une autre, et est identifiée relativement à cette autre entité. On parle alors *d'entité faible*.

Prenons l'exemple d'un cinéma, et de ses salles. On peut considérer chaque salle comme une entité, dotée d'attributs comme la capacité, l'équipement en son Dolby, ou autre. Il est difficilement imaginable de représenter une salle sans qu'elle soit rattachée à son cinéma. C'est en effet au niveau du cinéma que l'on va trouver quelques informations
générales comme l'adresse de la salle.


[![figure29 ](faible.png )](faible.png )

> Fig. 29 Modélisations possibles du lien Cinéma-Salle

Il est possible de représenter le lien entre un cinéma et ses salles par une association classique, comme le montre la Fig.29.a. La cardinalité 1..1 force la participation d'une
salle à un lien d'association avec un et un seul cinéma. Cette
représentation est correcte, mais présente un (léger) inconvénient : on doit créer un identifiant artificiel `id` pour le type d'entité *Salle*, et numéroter toutes les salles, *indépendamment du cinéma auquel elles sont rattachées*.

On peut considérer qu'il est plus naturel de numéroter les salles par un numéro interne à chaque cinéma. La clé d'identification d'une salle est alors constituée de deux parties :

> -   la clé de *Cinéma*, qui indique dans quel cinéma se trouve la
>     salle ;
> -   le numéro de la salle au sein du cinéma.

En d'autres termes, l'entité *Salle* ne dispose pas d'une
identification absolue, mais d'une identification *relative* à une autre entité. Bien entendu cela force la salle à toujours être associée à un et un seul cinéma.

La représentation graphique des entités faibles avec UML est illustrée dans la Figure 29.b. La salle est associée au cinéma avec une association qualifiée par l'attribut `no`
qui sert de discriminant pour distinguer les salles au sein d'un même cinéma. Noter que la cardinalité du côté *Cinéma* est implicitement 1..1.

L'introduction d'entités faibles est un subtilité qui permet de
capturer une caractéristique intéressante du modèle. Elle n'est pas une nécessité absolue puisqu'on peut très bien utiliser une association classique. La principale différence est que, dans le cas d'une entité faible, on obtient une identification composée qui peut être plus pratique à gérer, et peut également rendre plus faciles certaines requêtes. On touche ici à la liberté de choix qui est laissée, sur bien
des aspects, à un "modeleur" de base de données, et qui nécessite de s'appuyer sur une expérience robuste pour apprécier les conséquences de telle ou telle décision.

La présence d'un type d'entité faible $B$ associé à un type d'entité $A$ implique également des contraintes fortes sur les créations, modifications et destructions des instances de $A$ car on doit toujours s'assurer que la contrainte est valide. Concrètement, en prenant l'exemple de *Salle* et de *Cinéma*, on doit mettre en place les mécanismes suivants :

> -   quand on insère une salle dans la base, on doit toujours
>     l'associer à un cinéma ;
> -   quand un cinéma est détruit, on doit aussi détruire toutes ses
>     salles ;
> -   quand on modifie la clé d'un cinéma, il faut répercuter la
>     modification sur toutes ses salles (mais il est préférable de ne
>     jamais avoir à modifier une clé).

Réfléchissez bien à ces mécanismes pour apprécier le sucroît de
contraintes apporté par des variantes des associations. Parmi les impacts qui en découlent, et pour respecter les règles de
destruction/création énoncées, on doit mettre en place une stratégie.
Nous verrons que les SGBD relationnels nous permettent de spécifier de telles stratégies.

### Associations généralisées


On peut envisager des associations entre plus de deux entités, mais elles sont plus difficiles à comprendre, et surtout la signification des cardinalités devient beaucoup plus ambiguë. Prenons l'exemple d'une association permettant de représenter la projection de certains films dans des salles. Une association plusieurs-à-plusieurs entre *Film* et
*Salle* semble a priori faire l'affaire, mais dans ce cas
l'identifiant de chaque lien est la paire constituée
`(idFilm, idSalle)`. Cela interdit de projeter plusieurs fois le même film dans la même salle, ce qui pour le coup est inacceptable.

Il faut donc introduire une information supplémentaire, par exemple l'horaire, et définir association ternaire entre les types d'entités *Film*, *Salle* et *Horaire*. On obtient la Fig.30.

[![figure30 ](assoc-tern.png  )](assoc-tern.png )

> *Fig. 30* Association ternaire représentant les séances

On tombe alors dans plusieurs complications. Tout d'abord les
cardinalités sont, implicitement, 0..\*. Il n'est pas possible de dire qu'une entité ne participe qu'une fois à l'association. Il est vrai que, d'une part la situation se présente rarement, d'autre part cette limitation est due à la notation UML qui place les cardinalités à l'extrémité opposée d'une entité.

Plus problématique en revanche est la détermination de la clé.
Qu'est-ce qui identifie un lien entre trois entités ? En principe, la clé est le triplet constitué des clés respectives de la salle, du film et de l'horaire constituant le lien. On aurait donc le $n$-uplet *\[nomCinéma, noSalle, idFilm, idHoraire\]*. Une telle clé ne permet pas d'imposer certaines contraintes comme, par exemple, le fait que dans une salle, pour un horaire donné, il n'y a qu'un seul film.

Ajouter une telle contrainte, c'est signifier que la clé de
l'association est en fait constitué de *\[nomCinéma, noSalle,
idHoraire\]*. C'est donc un sous-ensemble de la concaténation des clés, ce qui semble rompre avec la définition donnée précédemment. Inutile de développer plus : les associations de degré supérieur à deux sont difficiles à manipuler et à interpréter. Il est *toujours* possible d'utiliser le mécanisme de réification déjà énoncé et de remplacer
cette association par un type d'entité. Pour cela on suit la règle suivante :


> **Règle de réification**   
> Soit $A$ une association entre les types d'entité $\{E_1, E_2, \ldots, E_n\}$. La transformation de $A$ en type d'entité $E_A$ s'effectue en deux étapes :  
> -   On attribue un identifiant autonome à $E_A$.
> -   On crée une association $A_i$ de type '(0..n):(1..1)' entre $E_A$ et chacun des $E_i$.  
> Chaque instance de $E_A$ doit impérativement être associée à une instance (et une seule) de chacun des $E_i$. Cette contrainte de participation (le "1..1") est héritée du fait que l'association initiale ne pouvait exister que comme lien créé entre les entités participantes.


L'association précédente peut être transformée en un type d'entité *Séance*. On lui attribue un identifiant `idSéance`, et des associations '(0..n):(1..1)' avec *Film*, *Horaire* et *Salle*. Pour chaque séance, on doit impérativement connaître le fillm, la salle et l'horaire. On obtient le schéma de la Fig. 31.

[![figure31 ](seance.png  )](seance.png )

> *Fig. 31.* L'association *Séance* transformée en entité

On peut ensuite ajouter des contraintes supplémentaires, indépendantes de la clé, pour dire par exemple qu'il ne peut y avoir qu'un film dans une salle pour un horaire. Nous étudierons ces contraintes dans le prochaine chapitre.

### Spécialisation


La modélisation UML est basée sur le modèle orienté-objet dont l'un des principaux concepts est *la spécialisation*. On est ici au point le plus divergent des représentations relationnelle et orienté-objet, puisque la spécialisation n'existe pas dans la première, alors qu'il est au cœur
des modélisations avancées dans la seconde.

Il n'est pas très fréquent d'avoir à gérer une situation impliquant de la spécialisation dans une base de données. Un cas sans doute plus courant est celui où on effectue la modélisation objet d'une application dont on souhaite rendre les données persistantes.

> **Note**
> Cette partie présente des notions assez avancées qui sont introduites pour des raisons de complétude mais peuvent sans trop de dommage être ignorées dans un premier temps.


Voici quelques brèves indications, en prenant comme exemple illustratif le cas très simple d'un raffinement de notre modèle de données. La notion plus générale de *vidéo* est introduite, et un film devient un cas particulier de vidéo. Un autre cas particulier est le *reportage*, ce qui donne donc le modèle de la Fig. 32. Au niveau de la super-classe, on trouve le titre et l'année. Un film se distingue par l'association à des acteurs et un metteur en scène ; un reportage en revanche a un lieu de tournage et une date.


[![figure32 ](heritage.png  )](heritage.png )

> *Fig. 32.* Notre exemple d'héritage



Le type d'entité *Vidéo* factorise les propriétés commune à toutes les vidéos, quel que soit leur type particulier (film, ou reportage, ou dessin animé, ou n'importe quoi d'autre). Au niveau de chaque sous-type, on trouve les propriétés particulières, par exemple l'association avec les acteurs pour un film (qui n'a pas de sens pour un reportage ou un dessin animé).

La particularité de cette modélisation est qu'une entité (par exemple un film) voit sa représentation éclatée selon deux types d'entité, cas que nous n'avons pas encore rencontré.

### Bilan


Le modèle E/A est l'outil universellement utilisé pour modéliser une base de données. Il présente malheureusement plusieurs limitations, qui découlent du fait que beaucoup de choix de conceptions plus ou moins équivalents peuvent découler d'une même spécification, et que la spécification elle-même est dans la plupart du cas informelle et sujette
à interprétation.

Un autre inconvénient du modèle E/A reste sa pauvreté : il est difficile d'exprimer des contraintes d'intégrité, des structures complexes.
Beaucoup d'extensions ont été proposées, mais la conception de schéma reste en partie matière de bon sens et d'expérience. On essaie en général :

> -   de se ramener à des associations entre 2 entités : au-delà, on a
>     probablement intérêt à transformer l'association en entité ;
> -   d'éviter toute redondance : une information doit se trouver en un
>     seul endroit ;
> -   enfin \-- et surtout \-- de privilégier la simplicité et la
>     lisibilité, notamment en ne représentant que ce qui est
>     strictement nécessaire.

La mise au point d'un modèle engage fortement la suite d'un projet de développement de base de données. Elle doit s'appuyer sur des personnes expérimentées, sur l'écoute des prescripteurs, et sur un processus par itération qui identifie les ambiguités et cherche à les résoudre en
précisant le besoin correspondant.

Dans le cadre des bases de données, le modèle E/A est utilisé dans la phase de conception. Il permet de spécifier tout ce qui est nécessaire pour construire un schéma de base de données normalisé, comme expliqué dans la prochaine session.

