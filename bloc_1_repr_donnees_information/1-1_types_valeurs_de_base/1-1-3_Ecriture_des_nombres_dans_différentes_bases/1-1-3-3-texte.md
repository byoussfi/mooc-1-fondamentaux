Comme la base $2$ est celle qui nous sera la plus utile dans la suite,
nous allons maintenant nous intéresser aux différentes opérations que
nous pouvons appliquer aux nombres dans cette base.

#### Opérations arithmétiques

On peut utiliser l’addition, la soustraction, la multiplication et la
division euclidienne sur les nombres dans n’importe quelle base, en
particulier la base $2$. Pour l’addition, on procède en base $2$ comme
en base $10$, en faisant d’abord la somme des unités, puis des chiffres
correspondant au poids $2^1$ (au lieu de $10^1$ en base $10$), $2^2$,
*etc*…Il faut simplement être attentif au fait qu’un report a lieu dès
que la somme dépasse $2_{10}=10_2$. Par exemple : $1+0$ donne une somme
de $1$, et un report de $0$. Par contre, $1+1=10_2$ donne une somme de
$0$ et un report de $1$. De même, $1+1+1=11_2$ donne une somme de $1$ et
un report de $1$. Par exemple :

**Exemple :**

$$\begin{array}{cccccc}
  &{}^1&{}^1&{}^1&{}^1\\
  & &1&0&1&1\\
  +& &1&1&0&1\\
  \hline
  &1&1&0&0&0  
\end{array}$$

$\blacksquare$

#### Opérations logiques

En base 2, on peut utiliser une série d’opérations qui n’ont pas
d’équivalent naturel dans les autres bases. Ce sont les opérations
logiques, que l’on peut appliquer en considérant que le 0 représente la
valeur logique faux, et que le 1 représente la valeur logique vrai.

On définit les opérateurs binaires[^1] **et**$\;$ (noté $\wedge$),
**ou**$\;$ (noté $\vee$), et **ou exclusif**$\;$ (noté
$\,\mathit{XOR}\,$), ainsi que l’opérateur unaire **non**$\;$ (noté
$\neg$). Pour définir leur effet sur deux nombres binaires, on commence
par les définir sur deux bits (ou un bit dans le cas du $\neg$). Pour ce
faire, on utilise une *table de vérité*, qui donne, pour chaque valeur
possible des opérandes (de l’opérande), la valeur renvoyée par
l’opérateur. Voici les tables de vérité de ces opérateurs :

**Définition :**

<div class="center" markdown="1">

| $x$ | $y$ | $x\wedge y$ |
|:---:|:---:|:-----------:|
|  0  |  0  |      0      |
|  0  |  1  |      0      |
|  1  |  0  |      0      |
|  1  |  1  |      1      |

| $x$ | $y$ | $x\vee y$ |
|:---:|:---:|:---------:|
|  0  |  0  |     0     |
|  0  |  1  |     1     |
|  1  |  0  |     1     |
|  1  |  1  |     1     |

| $x$ | $y$ | $x \,\mathit{XOR}\, y$ |
|:---:|:---:|:----------------------:|
|  0  |  0  |           0            |
|  0  |  1  |           1            |
|  1  |  0  |           1            |
|  1  |  1  |           0            |

| $x$ | $\neg x$ |
|:---:|:--------:|
|  0  |    1     |
|  1  |    0     |

</div>

$\square$

On remarquera que ces opérateurs expriment l’idée intuitive qu’on peut
s’en faire à la lecture de leur nom. L’opération $x\land y$ vaut $1$ si
et seulement $x$ *et* $y$ sont vraies (soit $x=1$ et $y=1$). L’opération
$x\lor y$ vaut $1$ si et seulement si $x$ *ou* $y$ est vraie.
L’opération $x \,\mathit{XOR}\, y$ vaut $1$ si et seulement si soit $x$
est vraie soit $y$ est vraie mais pas les deux en même temps (si $x$ est
vraie, cela *exclut* donc le fait que $y$ soit vraie et vice-versa).
Finalement, $\neg x$ remplace la valeur de vérité de $x$ par son opposé
(vrai par faux et faux par vrai).

On peut maintenant généraliser ces opérations à n’importe quel nombre
binaire, en appliquant les opérations *bit à bit* : étant donné deux
nombres sur $n$ bits $A=a_{n-1}a_{n-2}\cdots a_0$ et
$B=b_{n-1}b_{n-2}\cdots b_0$, on applique l’opération sur chaque paire
de bits $a_i$, $b_i$ pour obtenir le bit $c_i$ du résultat. Par exemple,
$A\wedge B$ est le nombre $C=c_{n-1}c_{n-2}\cdots c_0$, où
$c_{n-1}=a_{n-1}\wedge b_{n-1}$, $c_{n-2}=a_{n-2}\wedge b_{n-2}$,…et
$c_0=a_0\wedge b_0$. Et ainsi de suite pour les autres opérations.

#### Masques

Ces opérations logiques permettent de réaliser certaines manipulations
des valeurs binaires, appelées *masques* ou *masquages*. On observe que
$x\wedge 0=0$ et $x\wedge 1=x$, quelque soit la valeur de $x$ (sur un
bit). De ce fait, le $\wedge$ peut être utilisé pour *masquer* certains
bit d’un nombre, c’est-à-dire remplacer ces bits par des 0 tout en
conservant les autres. Il suffit de prendre la conjonction de ce nombre
avec une valeur binaire comprenant des $0$ à toutes les positions qu’on
veut masquer, et des $1$ partout ailleurs.

**Exemple :**

Supposons qu’on souhaite masquer les 4 bits de poids fort d’un nombre
$N$ de 8 bits. On peut faire: $N\wedge 0000\ 1111$. Si $N=1010\ 1010$,
on a bien $N \wedge 0000\ 1111=0000\ 1010$.

$\blacksquare$

Les masques peuvent être très utiles. On peut déjà observer que les
masques permettent de calculer le reste d’une division entière par une
puissance de $2$. En effet, nous avons déjà vu plus haut que le reste de
la division entière de $N$ par $2^k$ est le nombre composé des $k$ bits
de poids faible de $N$. On peut donc masquer les autres bits pour ne
retenir que ces $k$ bits.

**Exemple :**

Par exemple :

$$\begin{aligned}
    1010\ 0101_2 \bmod 8_{10} &= &1010\ 0101_2 \bmod 2^3\\
                              &= &1010\ 0101_2 \wedge 0000\ 0\underbrace{111}_3\\
                              &= &0000\ 0101_2\\
                              &= &101_2.
  \end{aligned}$$

$\blacksquare$

#### Décalages, divisions et multiplications

Une autre opération utile (mais qui n’est pas réservée aux nombres
binaires) est l’opération de *décalage de $n$ positions* (où $n$ est un
naturel). Il y a deux types de décalage :

-   le décalage vers la gauche de $n$ positions, d’un nombre $b$ est
    noté $b<<n$, et consiste à ajouter $n$ zéros à la droite (chiffres
    de poids faible) du nombre (quitte à supprimer les $n$ chiffres de
    poids forts pour garder le même nombre de bits). Les chiffres
    constituant le nombre initial sont donc bien décalés vers la gauche.

-   le décalage vers la droite de $n$ positions, d’un nombre $b$ est
    noté $b>>n$, et consiste à supprimer les $n$ chiffres de poids
    faibles du nombre $n$ (quitte à ajouter des $0$ à gauche pour garder
    le même nombre de bits)

**Exemple :**

Par exemple, en binaire: $0010\ 1101 >> 3 = 0000\ 0101$. De même
$0010\ 1101 << 2 = 1011\ 0100$.

$\blacksquare$

Comme nous l’avons déjà vu plus haut, ces opérations permettent
d’effectuer des multiplications (décalage vers la gauche de $k$
positions) et des divisions (décalage vers la droite de $k$ positions)
par $2^k$ (ou, de manière générale, par $b^k$ dans n’importe quelle base
$b$).

**Exemple :**

Par exemple :

-   $45_{10}\times 10_{10}= 45_{10} \times 10_{10}^1 = 45_{10} << 1 = 450_{10}$

-   $531_{10}\div 10_{10}= 531 >> 1 = 53_{10}$

-   $100010_2\div 4_{10}= 100010 \div 2^2 = 100010 >> 2 = 1000_2$

$\blacksquare$

[^1]: Attention ! Ici, binaire signifie que l’opérateur porte sur deux
    opérandes, comme l’addition par exemple. Au contraire, un opérateur
    unaire porte sur un seul opérande (comme le *moins* dans $-5$ par
    exemple).
