# Écriture des nombres dans différentes bases 5/5 : Nombres avec partie fractionnaire

1. Introduction : la notion de base
2. Changements de base
3. Manipuler les données en binaire
4. Nombres signés
5. **Nombres avec partie fractionnaire**

[![Vidéo 5 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S8.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S8.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
