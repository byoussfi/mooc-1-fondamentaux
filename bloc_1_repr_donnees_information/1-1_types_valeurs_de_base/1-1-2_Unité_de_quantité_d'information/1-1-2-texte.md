Commençons par fixer des unités permettant de mesurer la *quantité*
d’information. Nous utiliserons la taille de la représentation binaire
de l’information comme mesure:

**Définition :**

Chaque chiffre (0 ou 1) d’une représentation binaire est appelé **bit**
(contraction de l’anglais *binary digit*, ou chiffre binaire). Une
séquence de 8 bits est appelée un **octet** (ou **byte** en anglais).

$\square$

Les symboles associés à ces unités sont les suivants (SCC14.3 - Unit
Symbols Subcommittee 2004):

| Unité | Symbole |
|:-----:|:-------:|
|  bit  |    b    |
| octet |    o    |
| byte  |    B    |

Nous mesurerons donc l’information en terme de bits ou d’octets (bytes).
Naturellement, les quantités d’information manipulées usuellement
s’expriment en milliers, millions,… d’octets, il faut donc fixer des
noms pour ces unités. Les préfixes officiellement reconnus par
l’industrie (“Quantities and Units – Part 13: Information Science and
Technology” 2008) sont similaires aux kilo-, méga-,… du système SI. Ces
préfixes ne se réfèrent pas à des puissances de 10 comme dans le système
SI mais à des puissances de 2:

|    Nom    | Abréviation |                   Quantité                   |
|:---------:|:-----------:|:--------------------------------------------:|
| kibioctet |     kio     |          $2^{10}=1024\approx 10^3$           |
| mébioctet |     Mio     |       $2^{20}=1\ 048\ 576\approx 10^6$       |
| gibioctet |     Gio     |    $2^{30}=1\ 073\ 741\ 824\approx 10^9$     |
| tebioctet |     Tio     | $2^{40}=1 099\ 511\ 627\ 776\approx 10^{12}$ |

La définition de ces préfixes est relativement récente (elle date du
début des années 2000). En pratique, on est souvent confronté à
al’utilisation des préfixes traditionnels kilo-, méga-, tera-, *etc* du
système SI, qui sont, pour rappel:

|    Nom    | Abréviation |     Quantité     |
|:---------:|:-----------:|:----------------:|
| kilooctet |     ko      |  $10^3$ octets   |
| mégaoctet |     Mo      |  $10^6$ octets   |
| gigaoctet |     Go      |  $10^9$ octets   |
| teraoctet |     To      | $10^{12}$ octets |

On voit donc qu’un mégaoctet est un petit peu plus petit qu’un
mébioctet…Force est de reconnaître qu’une certaine confusion règne !
Cette confusion a d’ailleurs donné lieu à des procès retentissants,
notamment aux États-Unis, où des consommateurs ont entamé une *class
action* contre plusieurs fabricants de disques durs et mémoires, les
accusant de tromperie sur la capacité de stockage des produits[^1].

<div id="refs" class="references csl-bib-body hanging-indent"
markdown="1">

<div id="ref-iso" class="csl-entry" markdown="1">

“Quantities and Units – Part 13: Information Science and Technology.”
2008. International Organization for Standardization.
<https://www.iso.org/standard/31898.html>.

</div>

<div id="ref-ieeeunits" class="csl-entry" markdown="1">

SCC14.3 - Unit Symbols Subcommittee. 2004. “IEEE Standard Letter Symbols
for Units of Measurement (SI Customary Inch-Pound Units, and Certain
Other Units).” 260.1-2004. IEEE.
<https://standards.ieee.org/standard/260_1-2004.html>.

</div>

</div>

[^1]: Voir par exemple:
    <https://en.wikipedia.org/wiki/Binary_prefix#Inconsistent_use_of_units>.
