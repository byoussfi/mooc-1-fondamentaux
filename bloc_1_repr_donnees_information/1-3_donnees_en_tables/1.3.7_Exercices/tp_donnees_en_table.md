# Manipulation de données en tables

Le fichier [`nat2020.csv`](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/tables/nat2020.csv?inline=false) est un jeu de données ouvertes concernant les prénoms donnés en France de 1900 à 2020. Nous allons réaliser quelques manipulations sur ces données, puis répondre au quiz pour vérifier nos résultats.

Les données simplifiées sont disponibles au téléchargement depuis le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/fichier-des-prenoms-de-1900-a-2019/) 

**Attention** malgré le titre qui annonce 2019, les années vont bien jusqu'en 2020.

## Lire le fichier et stocker les données dans une structure

Nous allons utiliser le module Python `csv`  pour lire notre fichier et stocker les informations dans une liste de dictionnaires.


```python
import csv
```

### Question 1

1. Ouvrir le fichier avec l'aide de l'outil de votre choix (éditeur de texte ou tableur) et vérifier que les données sont lisibles (pas de souci d'encodage), et noter la présence des descripteurs.

2. Définir des constantes explicites qui prennent les valeurs de ces descripteurs et les ranger dans un tuple, dans l'ordre adéquat.


### Question 2

1. Écrire une fonction `lecture` qui prend en paramètres :
    - un nom de fichier (type `str`) contenant des données au format `csv`  
    - un `delimiter` (type `str`) qui pourra être le point-virgule par défaut `';'`)
    - un ` encoding` (type ` str`) qui pourra être ` 'utf-8'`  par défaut

Et qui retourne la liste des dictionnaires des données. On utilisera l'appel simplifié suivant à la fonction `csv.DictReader` :

```python
csv.DictReader(csvfile, delimiter=delimiter)
``` 

2. En utilisant votre fonction de lecture, définir une constante `TABLE_PRENOMS` contenant l'ensemble des données. Combien d'éléments comporte cette liste ?


## Réaliser quelques requêtes

### Question 3

La première information que l'on souhaite pouvoir extraire des données est : combien de fois un prénom a été attribué une certaine année ? Écrire un fonction `compte` qui prend un prénom et une année en paramètres et répond à la question en renvoyant un entier.

_Attention_ : faire en sorte que la fonction ne soit pas sensible à la casse.


Tester votre fonction sur quelques exemples. Combien de fois votre prénom a-t-il été donné l'année de votre naissance ?


### Question 4

Extraire la liste des prénoms féminins donnés en 2000. Quel est celui qui a été le plus donné ? On pourra utiliser la fonction ` sorted` de la sorte :

```python
sorted((ici la requête), key=lambda e: ici le critère de tri, reverse=True)
``` 

Et si on exclut `_PRENOMS_RARES` ?


### Question 5

Chaque année des _prénoms rares_ sont donnés. Quelle année le nombre de prénoms rares a-t-il été le plus faible ?

### Question 6

Le champion toute catégorie est probablement le prénom Marie. Mais combien de fois a-t-il été attribué entre 1900 et 2020 ?

