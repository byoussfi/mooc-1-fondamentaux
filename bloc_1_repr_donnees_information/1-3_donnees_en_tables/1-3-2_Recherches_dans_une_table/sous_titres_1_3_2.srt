1
00:00:04,400 --> 00:00:06,513
Nous nous retrouvons donc pour manipuler nos données.

2
00:00:06,613 --> 00:00:09,496
Une des manipulations sur les données que nous pouvons faire

3
00:00:09,596 --> 00:00:10,699
sont évidemment des recherches.

4
00:00:11,884 --> 00:00:15,684
Nous reprenons nos deux tables

5
00:00:15,784 --> 00:00:17,674
sur les pays et sur les villes.

6
00:00:17,774 --> 00:00:19,429
Nous allons essentiellement manipuler

7
00:00:19,529 --> 00:00:22,428
la table des pays qui est plus petite.

8
00:00:24,385 --> 00:00:27,709
Voilà, nous avons à nouveau lu les données

9
00:00:27,809 --> 00:00:29,306
comme vu précédemment

10
00:00:29,406 --> 00:00:31,860
et nous allons maintenant effectuer une recherche.

11
00:00:31,960 --> 00:00:35,024
Une première recherche consiste en une recherche simple

12
00:00:35,124 --> 00:00:36,516
sur la valeur d'un descripteur.

13
00:00:36,616 --> 00:00:38,724
Nous cherchons ici par exemple exactement

14
00:00:39,603 --> 00:00:41,379
le ou les enregistrements

15
00:00:41,479 --> 00:00:44,051
qui ont comme valeur "Argentina"

16
00:00:44,151 --> 00:00:46,492
sur le descripteur "nom".

17
00:00:48,586 --> 00:00:51,239
Alors évidemment, dans cette table-là,

18
00:00:51,339 --> 00:00:52,310
il n'y a qu'une réponse

19
00:00:52,410 --> 00:00:57,978
mais il pourrait y en avoir plusieurs

20
00:00:58,078 --> 00:00:59,881
et on prend l'habitude en fait

21
00:00:59,981 --> 00:01:01,635
d'extraire les enregistrements

22
00:01:01,735 --> 00:01:04,833
et créer une sous-table ou une nouvelle table

23
00:01:04,933 --> 00:01:08,224
avec les enregistrements de notre recherche

24
00:01:08,842 --> 00:01:10,364
de la façon suivante

25
00:01:10,464 --> 00:01:13,552
donc en créant un tableau par compréhension

26
00:01:13,652 --> 00:01:17,833
et cette syntaxe, donc avec le for

27
00:01:17,933 --> 00:01:21,579
pour parcourir la table qui nous intéresse,

28
00:01:21,679 --> 00:01:24,654
et le if ici qui va permettre de rajouter la contrainte

29
00:01:24,754 --> 00:01:28,779
ici sur la valeur de notre descripteur.

30
00:01:30,431 --> 00:01:33,244
Et on verra que cette syntaxe finalement

31
00:01:33,344 --> 00:01:37,003
en tout cas, cette forme de requête

32
00:01:37,103 --> 00:01:40,759
va ressembler assez fortement à ce qu'on fera

33
00:01:40,859 --> 00:01:43,487
avec les SGBD et le langage SQL.

34
00:01:46,595 --> 00:01:52,275
Ici, on cherche les pays qui sont des dictionnaires

35
00:01:52,375 --> 00:01:54,591
on va récupérer l'ensemble des dictionnaires

36
00:01:54,691 --> 00:01:56,477
de notre table L_PAYS

37
00:01:56,577 --> 00:02:01,328
tels que le nom soit égal à "Argentine".

38
00:02:02,623 --> 00:02:07,290
et on obtient bien la liste avec un seul enregistrement.

39
00:02:07,390 --> 00:02:08,674
Et on peut effectivement

40
00:02:08,774 --> 00:02:12,385
en prenant le premier et le seul enregistrement,

41
00:02:12,485 --> 00:02:14,816
faire un filtre sur la capitale par exemple

42
00:02:14,916 --> 00:02:18,674
et obtenir la valeur associée au descripteur "capitale".

43
00:02:19,942 --> 00:02:23,824
On peut créer une fonction qui réalise cette recherche

44
00:02:23,924 --> 00:02:25,841
de façon un petit peu générique

45
00:02:25,941 --> 00:02:28,019
et pouvoir l'utiliser par la suite.

46
00:02:28,119 --> 00:02:32,045
On l'utilise avec notre requête initiale,

47
00:02:32,145 --> 00:02:34,218
ici, une deuxième recherche

48
00:02:34,318 --> 00:02:35,502
toujours sur notre table pays,

49
00:02:35,602 --> 00:02:37,172
avec le descripteur "continent"

50
00:02:37,272 --> 00:02:40,288
et comme valeur, la chaîne de caractères "EU"

51
00:02:40,388 --> 00:02:42,277
pour avoir  les pays européens.

52
00:02:43,755 --> 00:02:45,032
D'autres recherches sont possibles

53
00:02:45,966 --> 00:02:48,938
Ici, on recherche les pays de plus de 50 millions d'habitants

54
00:02:49,434 --> 00:02:52,899
on cherche toujours sur la table L_PAYS

55
00:02:52,999 --> 00:02:54,971
mais cette fois, ce qu'on veut,

56
00:02:55,071 --> 00:02:59,742
c'est que la valeur associée au descripteur "pop"

57
00:02:59,842 --> 00:03:01,656
population, soit supérieur à 50 millions.

58
00:03:01,756 --> 00:03:07,378
Attention, lorsqu'on récupère des données en table avec Python,

59
00:03:08,084 --> 00:03:10,899
toutes les données vont être au format str,

60
00:03:10,999 --> 00:03:12,120
au format chaîne de caractères.

61
00:03:12,745 --> 00:03:15,248
Il va falloir convertir ces données

62
00:03:15,348 --> 00:03:17,444
c'est le rôle ici de la fonction int()

63
00:03:17,544 --> 00:03:19,238
convertir les données

64
00:03:19,338 --> 00:03:22,284
qu'on a récupérées sous la forme d'une chaîne de caractères

65
00:03:22,384 --> 00:03:23,821
au format adéquat

66
00:03:23,921 --> 00:03:26,814
en fonction de la recherche qu'on veut.

67
00:03:26,914 --> 00:03:29,657
Ici, on veut bien transformer

68
00:03:30,615 --> 00:03:32,150
la chaîne de caractères qui correspond

69
00:03:32,250 --> 00:03:34,554
à la valeur associée au descripteur "pop"

70
00:03:34,654 --> 00:03:38,339
en entier pour pouvoir faire notre comparaison avec 50 millions.

71
00:03:43,275 --> 00:03:44,674
Donc 28 pays

72
00:03:44,774 --> 00:03:48,316
sont peuplés de plus de 50 millions d'habitants.

73
00:03:48,969 --> 00:03:51,444
Encore une fois, on a extrait

74
00:03:51,544 --> 00:03:53,626
de la table pays

75
00:03:53,726 --> 00:03:57,178
on a extrait un certain nombre d'enregistrements

76
00:03:57,278 --> 00:03:59,515
qui vérifient le critère voulu.

77
00:04:01,423 --> 00:04:04,687
On peut créer une fonction un peu plus générique

78
00:04:06,368 --> 00:04:07,644
sans rentrer dans le détail

79
00:04:07,744 --> 00:04:09,732
qui prend plusieurs paramètres,

80
00:04:09,832 --> 00:04:11,163
la table, le descripteur,

81
00:04:11,263 --> 00:04:12,677
le type du descripteur justement

82
00:04:12,777 --> 00:04:16,127
pour pouvoir pallier le fait que certains descripteurs

83
00:04:16,227 --> 00:04:18,542
les champs ne sont pas forcément des chaînes de caractères

84
00:04:18,642 --> 00:04:21,048
mais d'autres types, int, float, et cætera,

85
00:04:21,148 --> 00:04:24,047
la valeur recherchée et éventuellement un opérateur

86
00:04:24,147 --> 00:04:25,466
qui peut être l'égalité

87
00:04:25,566 --> 00:04:28,545
comme c'était le cas sur les premières recherches

88
00:04:28,645 --> 00:04:30,309
mais qui peut être un autre opérateur.

89
00:04:32,538 --> 00:04:35,361
Nous allons tester cette fonction

90
00:04:35,461 --> 00:04:37,884
sur différentes recherches.

91
00:04:38,476 --> 00:04:40,528
Nous reprenons la recherche de nos pays peuplés

92
00:04:40,628 --> 00:04:44,527
en utilisant cette fois cette fonction générique.

93
00:04:44,627 --> 00:04:47,423
Nous obtenons bien sûr le même résultat.

94
00:04:47,697 --> 00:04:50,006
Nous cherchons maintenant les pays d'Europe

95
00:04:50,106 --> 00:04:54,125
avec moins de 10000 km2

96
00:04:54,715 --> 00:05:00,061
Nous pouvons commencer par rechercher les petits pays

97
00:05:00,161 --> 00:05:01,939
donc ici, nous avons porté la recherche 

98
00:05:02,039 --> 00:05:05,275
sur l'ensemble de la table L_PAYS

99
00:05:05,375 --> 00:05:09,519
80 pays inférieurs à 10000 km2

100
00:05:09,619 --> 00:05:10,580
et parmi ces pays,

101
00:05:10,680 --> 00:05:12,951
nous pouvons faire effectivement une deuxième recherche

102
00:05:13,051 --> 00:05:16,972
pour ne rechercher que ceux qui viennent d'Europe.

103
00:05:17,072 --> 00:05:20,974
Nous ré-utilisons notre fonction recherche qui est un peu générique

104
00:05:21,074 --> 00:05:24,575
cette fois sur la table PETITS_PAYS que nous venons de créer

105
00:05:25,459 --> 00:05:30,027
en faisant une recherche sur le descripteur "continent".

106
00:05:35,810 --> 00:05:39,329
Donc voilà un petit peu les différentes recherches

107
00:05:39,429 --> 00:05:41,063
que nous pouvons effectuer sur notre table

108
00:05:41,163 --> 00:05:43,055
donc soit des recherches simples

109
00:05:43,155 --> 00:05:45,910
avec égalité d'une valeur sur un descripteur

110
00:05:46,010 --> 00:05:48,358
soit des recherches un petit peu plus complexes

111
00:05:48,458 --> 00:05:50,295
avec d'autres opérateurs.

112
00:05:50,763 --> 00:05:56,934
Bien sûr, nous pouvons combiner par des opérateurs booléens

113
00:05:57,034 --> 00:06:00,273
les différentes contraintes.

114
00:06:00,891 --> 00:06:02,433
Ici, par exemple,

115
00:06:02,533 --> 00:06:04,825
au lieu de faire la recherche en deux étapes,

116
00:06:04,925 --> 00:06:07,263
nous aurions pu la combiner

117
00:06:07,363 --> 00:06:09,570
en utilisant un et.

118
00:06:09,670 --> 00:06:12,569
Bien sûr, la fonction générique n'était pas prévue pour,

119
00:06:13,653 --> 00:06:15,620
elle est prévue pour un seul opérateur

120
00:06:15,720 --> 00:06:19,663
mais nous aurions pu faire cette recherche

121
00:06:19,763 --> 00:06:25,169
directement sans utiliser cette fonction.

122
00:06:28,405 --> 00:06:29,793
On peut le faire d'ailleurs.

123
00:06:30,160 --> 00:06:35,219
Donc ici, si on cherche l'ensemble des pays européens

124
00:06:37,756 --> 00:06:41,636
qui sont de moins de 10000 km2,

125
00:06:48,342 --> 00:06:49,842
donc avec deux contraintes,

126
00:06:49,942 --> 00:06:57,708
nous voulons que la surface du pays

127
00:06:57,808 --> 00:07:00,315
soit plus petite que 10000.

128
00:07:00,415 --> 00:07:04,010
Il s'agit d'un float

129
00:07:14,870 --> 00:07:17,183
et, donc l'opérateur booléen,

130
00:07:17,383 --> 00:07:20,501
nous voulons que le continent soit européen.

131
00:07:34,034 --> 00:07:35,080
Voilà.

132
00:07:44,785 --> 00:07:48,836
Nous allons référencer notre table avec une variable

133
00:07:48,936 --> 00:07:52,626
et nous obtenons bien 14 pays comme précédemment

134
00:07:52,726 --> 00:07:55,697
mais cette fois, nous avons fait la recherche en une seule étape

135
00:07:55,797 --> 00:07:57,377
en combinant nos deux critères.

136
00:07:59,341 --> 00:08:01,908
Voilà, je vous dis à bientôt

137
00:08:02,008 --> 00:08:05,181
pour d'autres manipulations sur les données en table

138
00:08:05,281 --> 00:08:07,927
et notamment, voir comment nous pouvons trier

139
00:08:08,027 --> 00:08:09,146
et fusionner des tables.

