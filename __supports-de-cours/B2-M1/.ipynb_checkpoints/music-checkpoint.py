class Duree:

    @classmethod
    def duree(cls, duree_str, delimiter=':'):
        h, m, s = 0, 0, 0
        if delimiter in duree_str:
            duree_liste = duree_str.strip().split(delimiter)
            if len(duree_liste) == 3:
                h, m, s = duree_liste
            elif len(duree_liste) == 2:
                m, s = duree_liste
                h = '0'
            elif len(duree_liste) == 1:
                s = duree_liste
                h, m = '0', '0'
            else:
                h, m, s = '0', '0', '0'
            if h.isdigit() and m.isdigit() and s.isdigit():
                h, m, s = int(h), int(m), int(s)
        return Duree(h, m, s)


    # Réponse à la question 1
    #
    def __init__(self, heure, mn, sec):
        if sec >= 60:
            mn += sec // 60
            sec = sec % 60
        if mn >= 60:
            heure += mn // 60
            mn = mn % 60
        self.heure = heure
        self.min = mn
        self.sec = sec
    # --------------------------
    
    def __str__(self):
        heure_str = f'{self.heure}:' if self.heure > 0 else ''
        return f'{heure_str}{self.min:02}:{self.sec:02}'
    
    def __repr__(self):
        return f'Duree({self.heure}, {self.min}, {self.sec})'

    # Réponses à la question 2
    #
    def __lt__(self, d):
        return (self.heure, self.min, self.sec) < (d.heure, d.min, d.sec) 

    def __eq__(self, d):
        return (self.heure, self.min, self.sec) == (d.heure, d.min, d.sec) 

    def __gt__(self, d):
        return (self.heure, self.min, self.sec) > (d.heure, d.min, d.sec) 
    # ------------------------

    # Réponse à la question 3
    #
    def __add__(self, d):
        return Duree(self.heure + d.heure, self.min + d.min, self.sec + d.sec)
    # -------------------------

# Les objets Titre et PlayList pour la fin de l'exercice
#  
class Titre:

    def __init__(self, annee, album, titre, duree):
        self.annee = annee
        self.album = album
        self.titre = titre
        self.duree = duree

    def __str__(self):
        return f'[{str(self.duree)}] {self.titre} ({self.album}, {self.annee})'
        

class PlayList:

    def __init__(self, base, ids):
        self.base = base
        self.ids = set(ids)

    def __str__(self):
        width = len(str(len(self.base)))
        return '\n'.join([f'{sid:{width}} {str(self.titre(sid))}' for sid in self.ids])


    def __len__(self):
        return len(self.ids)
    
    def titre(self, id):
        return self.base[id]
    
    def duree(self):
        return sum((self.titre(id).duree for id in self.ids), start=Duree(0, 0, 0))
    
    def commun(self, playlist):
        return PlayList(self.base, self.ids & playlist.ids)



# Pour la question 4
#
def lecture(filename, delimiter=';'):
    """Lit le fichier texte contenant les titres au format csv et retourne une liste de Titre"""
    liste_de_titres = []
    with open(filename, 'r', encoding='utf-8') as datas:
        for line in datas:
            annee, album, titre, duree_str = line.strip().split(delimiter)
            liste_de_titres.append(Titre(annee, album, titre, Duree.duree(duree_str)))
    return liste_de_titres

PINK_FLOYD = lecture('pink_floyd_durees.csv')
# -------------------------------

LIST_OF_ID = list(range(len(PINK_FLOYD)))

#XUAN = {60, 45, 107, 10, 51, 5, 30, 83, 94, 22, 4, 136, 145, 52, 133, 125, 86, 31, 87, 118, 82, 32, 43, 3, 27, 97, 150, 79, 152, 114, 54, 53, 93, 80, 141, 18, 115, 105, 72, 142, 81, 149, 17, 104, 102, 39, 11, 36, 91, 147, 134, 84, 117, 15, 128, 89, 50, 113, 33, 61, 124, 23, 59, 40, 111, 26, 100, 112, 19, 135, 123, 44, 119, 62, 155, 78, 7, 110, 157, 98, 99, 34, 65, 58, 139, 77, 47, 46, 8, 88, 1, 49, 95, 16, 41}

#BOB = {129, 134, 58, 65, 67, 0, 102, 140, 74, 34, 85, 73, 28, 40, 56, 101, 12, 25, 35, 68, 39, 55, 124, 37, 26, 49, 59, 146, 108, 36, 106, 21, 3, 117, 123, 143, 100, 64, 9, 22, 156, 76, 19, 4, 122, 79, 109, 62, 113, 142, 89, 152, 1, 128, 43, 81, 126, 94, 135, 118, 7, 136, 141, 93, 11, 114, 20, 95, 155, 53, 138, 42, 2, 78, 61, 10, 32, 132, 5, 110, 125, 72, 45, 111, 92, 88, 145, 121, 149, 150, 51, 41, 57, 69, 98, 63, 104, 47, 90, 48, 24, 120, 31, 130, 70}

#INAYA = {57, 41, 29, 83, 72, 2, 38, 25, 132, 60, 14, 136, 140, 127, 152, 54, 13, 17, 16, 116, 119, 101, 133, 129, 95, 130, 18, 63, 15, 64, 156, 52, 39, 123, 10, 73, 157, 107, 7, 58, 103, 75, 154, 61, 86, 137, 87, 111, 12, 47, 24, 23, 8, 117, 35, 108, 150, 118, 44, 42, 26, 55, 3, 32, 30, 59, 97, 74, 67, 99, 69, 88, 135, 131, 46, 53, 128, 112, 145, 125, 82, 147, 71, 68, 93, 113, 36, 4, 141, 65, 40, 50, 20, 94, 19, 122, 56, 11, 49, 76, 34, 153, 79, 9, 0, 5, 28, 70, 138, 151, 110, 144, 77, 33, 22, 100, 90, 143, 155, 126}


XUAN = {139, 88, 107, 57, 14, 100, 44, 129, 45, 8, 53, 138, 125, 105, 151, 98, 80, 40, 131, 128, 29, 122, 134, 150, 17, 19, 90, 66, 15, 133, 99, 153, 124, 67, 59, 35, 112, 156, 7, 71, 117, 4, 91, 41, 38, 152, 49, 130, 140, 86, 60, 146, 145, 143, 6, 18, 115, 9, 3, 22, 31, 46, 118, 136, 12, 68, 33, 103, 74, 21, 32, 111, 54, 104, 27, 85, 10, 23, 34, 25}

BOB = {111, 45, 134, 145, 27, 99, 14, 100, 74, 131, 138, 66, 146, 59, 118, 23, 8, 139, 129, 117, 31, 17, 25, 80, 153, 41, 38, 12, 130, 21, 6, 136, 91, 19, 22, 125, 46, 133, 90, 124, 152, 33, 10, 104, 29, 44, 122, 57, 128, 3, 85, 71, 54, 68, 105, 40, 32, 35, 67, 156, 34, 7, 143, 150, 18, 4, 151, 49, 103, 107, 88, 112, 115, 53, 60, 86, 98, 15, 140, 9}

INAYA = {90, 7, 103, 134, 143, 118, 49, 152, 6, 117, 128, 10, 60, 46, 17, 33, 105, 80, 122, 4, 125, 40, 107, 74, 145, 23, 57, 15, 41, 68, 140, 
86, 35, 138, 22, 66, 98, 19, 21, 153, 54, 115, 139, 25, 100, 29, 136, 8, 146, 111, 34, 133, 156, 91, 27, 129, 44, 9, 59, 124, 53, 12, 67, 112, 150, 18, 85, 88, 31, 151, 71, 14, 45, 3, 104, 38, 32, 131, 99, 130}
# Réponses à la question 6
#
xuan = PlayList(PINK_FLOYD, XUAN)
bob = PlayList(PINK_FLOYD, BOB)
inaya = PlayList(PINK_FLOYD, INAYA)

xuan_bob = xuan.commun(bob)
ami_es = xuan_bob.commun(inaya)
# ---------------------------
