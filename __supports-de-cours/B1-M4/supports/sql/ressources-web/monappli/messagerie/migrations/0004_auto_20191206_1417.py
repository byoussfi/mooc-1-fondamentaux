# Generated by Django 2.2.8 on 2019-12-06 14:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('messagerie', '0003_auto_20181210_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='emetteur',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages_emis', to='messagerie.Contact'),
        ),
    ]
