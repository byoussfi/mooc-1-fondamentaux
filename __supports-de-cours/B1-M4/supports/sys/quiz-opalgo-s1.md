> -   Dire qu'une requête est déclarative, c'est dire que (indiquer les
>     phrases correctes) :
>
>     ::: {.eqt}
>     optimReq1
>
>     A)  `I`{.interpreted-text role="eqt"} La requête ne définit pas
>         précisément le résultat.
>     B)  `C`{.interpreted-text role="eqt"} La requête ne dit pas
>         comment calculer le résultat.
>     C)  `C`{.interpreted-text role="eqt"} La requête est indépendante
>         de l'organisation des données.
>     D)  `I`{.interpreted-text role="eqt"} La requête est une
>         expression de besoin en langage naturel.
>     :::
>
> -   Un plan d'exécution, c'est
>
>     ::: {.eqt}
>     optimReq2
>
>     A)  `I`{.interpreted-text role="eqt"} Un programme choisi parmi un
>         ensemble fixe et pré-défini de programmes proposés par le
>         système.
>     B)  `C`{.interpreted-text role="eqt"} Un programme produit à la
>         volée par le système pour chaque requête.
>     C)  `C`{.interpreted-text role="eqt"} Un arbre d'opérateurs
>         communicants entre eux.
>     :::
>
> -   L'optimisation de requêtes, c'est
>
>     ::: {.eqt}
>     optimReq3
>
>     A)  `I`{.interpreted-text role="eqt"} Modifier une requête SQL
>         pour qu'elle soit la plus efficace possible.
>     B)  `I`{.interpreted-text role="eqt"} Structurer les données pour
>         qu'elles soient adaptées aux requêtes soumises.
>     C)  `C`{.interpreted-text role="eqt"} Choisir, pour chaque
>         requête, la meilleure manière de l'exécuter.
>     :::
>
> -   Quelles sont les affirmations vraies parmi les suivantes,? (1 pt)
>
> > ::: {.eqt}
> > optimReq3
> >
> > A)  `C`{.interpreted-text role="eqt"} Le choix d\'un plan
> >     d\'exécution dépend de la mémoire RAM disponible.
> > B)  `I`{.interpreted-text role="eqt"} Le choix d\'un plan
> >     d\'exécution dépend de la forme de la requête SQL
> > C)  `C`{.interpreted-text role="eqt"} Le choix d\'un plan
> >     d\'exécution dépend de l\'existence d\'index
> > D)  `I`{.interpreted-text role="eqt"} Le choix d\'un plan
> >     d\'exécution dépend du langage de programmation utilisé.
> > :::
