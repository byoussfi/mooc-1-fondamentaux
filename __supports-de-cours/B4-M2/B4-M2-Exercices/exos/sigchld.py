#!/usr/bin/env python3

import os
import time
import signal

def handler(sig,f):
    print("---- Je suis %d et j'ai reçu SIGCHLD" % os.getpid())
    res = os.waitpid(-1, 0) #attend un fils quelconque
    status = res[1]
    if os.WIFEXITED(status):
        print(  '[père] mon fils', res[0],
                'a terminé normalement avec valeur :', os.WEXITSTATUS(status))
    else:
        print('[père] mon fils', res[0], 'a termine anormalement')
    
def main():
    print('[père] mon pid est ', os.getpid())
    signal.signal(signal.SIGCHLD,handler)

    newpid = os.fork()
    if newpid == 0:
        print('[fils] mon pid est ', os.getpid())
    else:
        newpid2 = os.fork()
        if newpid2 == 0:
            print('[fils] mon pid est ', os.getpid())
        else:
            for i in range(10):
                time.sleep(1)
                print('[père] itération ', i)
        
    print('j\'ai terminé', os.getpid())
    os._exit(0)

main()

