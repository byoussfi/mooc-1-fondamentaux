#!/usr/bin/env python3

import os
import time
import signal

counterSIGUSR=0
sentSIGUSR=0

def handler_sigusr(sig,f):
    global counterSIGUSR
    counterSIGUSR+=1
    print("received SIGUSR = %d\n" % counterSIGUSR)
    time.sleep(1)

def main():
    global sentSIGUSR
    print('Je me lance et je suis', os.getpid())
    signal.signal(signal.SIGUSR2,handler_sigusr)
    newpid=os.fork()
    if newpid==0:
        for i in range(5):
            os.kill(os.getppid(), signal.SIGUSR2)
            sentSIGUSR+=1
        print("sent %d SIGUSR \n" % sentSIGUSR)

        os._exit(0)
    
    os.waitpid(newpid,0)
    os._exit(0)

main()



