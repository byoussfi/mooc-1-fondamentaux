#!/usr/bin/env python3

import os
import time
import signal

NB_PROC=10

def handler(sig,f):
    while True:
        try:
            res=os.waitpid(-1, os.WNOHANG)
            if res[0]>0: #attend un fils quelconque
                status = res[1]
                print( "%d a terminé avec %d \n" %(res[0], os.WEXITSTATUS(status)))
        except ChildProcessError:
            break
            
def main():
    global NB_PROC
    
    print('[père] mon pid est ', os.getpid())
    signal.signal(signal.SIGCHLD,handler)

    for i in range(NB_PROC):
        newpid = os.fork()
        if newpid == 0:
            break
        
    print('j\'ai terminé', os.getpid())
    os._exit(0)

main()

