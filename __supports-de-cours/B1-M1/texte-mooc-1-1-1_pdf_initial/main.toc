\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Le\IeC {\c c}ons 2 \IeC {\`a} 4 -- Repr\IeC {\'e}sentation de l'information}{9}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Unit\IeC {\'e}s de quantit\IeC {\'e} d'information}{10}{section.1.1}% 
\contentsline {section}{\numberline {1.2}\'Ecriture des nombres dans diff\IeC {\'e}rentes bases}{11}{section.1.2}% 
\contentsline {paragraph}{\nonumberline Chiffres de poids fort, de poids faible}{13}{section*.4}% 
\contentsline {paragraph}{\nonumberline Ajouts de z\IeC {\'e}ros}{13}{section*.5}% 
\contentsline {paragraph}{\nonumberline Multiplier ou diviser par la base}{13}{section*.6}% 
\contentsline {paragraph}{\nonumberline Combien de nombres repr\IeC {\'e}sentables sur $n$ chiffres ?}{14}{section*.7}% 
\contentsline {subsection}{\numberline {1.2.1}Changements de base}{14}{subsection.1.2.1}% 
\contentsline {paragraph}{\nonumberline Cas des nombres entiers}{15}{section*.8}% 
\contentsline {paragraph}{\nonumberline Nombres r\IeC {\'e}els}{16}{section*.9}% 
\contentsline {subsection}{\numberline {1.2.2}Op\IeC {\'e}rations en base 2}{18}{subsection.1.2.2}% 
\contentsline {paragraph}{\nonumberline Op\IeC {\'e}rations arithm\IeC {\'e}tiques}{19}{section*.10}% 
\contentsline {paragraph}{\nonumberline Op\IeC {\'e}rations logiques}{19}{section*.11}% 
\contentsline {paragraph}{\nonumberline Masques}{20}{section*.12}% 
\contentsline {paragraph}{\nonumberline D\IeC {\'e}calages, division et multiplications}{20}{section*.13}% 
\contentsline {subsection}{\numberline {1.2.3}Repr\IeC {\'e}sentation des entiers non-sign\IeC {\'e}s}{21}{subsection.1.2.3}% 
\contentsline {subsection}{\numberline {1.2.4}Repr\IeC {\'e}sentation des nombres entiers sign\IeC {\'e}s}{21}{subsection.1.2.4}% 
\contentsline {paragraph}{\nonumberline Bit de signe}{22}{section*.14}% 
\contentsline {paragraph}{\nonumberline Compl\IeC {\'e}ment \IeC {\`a} 1}{22}{section*.15}% 
\contentsline {paragraph}{\nonumberline Compl\IeC {\'e}ment \IeC {\`a} 2}{23}{section*.16}% 
\contentsline {paragraph}{\nonumberline Exc\IeC {\`e}s \IeC {\`a} $K$}{24}{section*.17}% 
\contentsline {paragraph}{\nonumberline Comparaison des diff\IeC {\'e}rentes repr\IeC {\'e}sentation}{24}{section*.18}% 
\contentsline {subsection}{\numberline {1.2.5}Repr\IeC {\'e}sentation des nombres <<~r\IeC {\'e}els~>>}{25}{subsection.1.2.5}% 
\contentsline {paragraph}{\nonumberline Une premi\IeC {\`e}re technique: la virgule fixe}{25}{section*.20}% 
\contentsline {paragraph}{\nonumberline La virgule flottante: IEEE754}{26}{section*.21}% 
\contentsline {section}{\numberline {1.3}Repr\IeC {\'e}sentation des caract\IeC {\`e}res}{29}{section.1.3}% 
\contentsline {paragraph}{\nonumberline Codes historiques: \'Emile \textsc {Baudot} et les t\IeC {\'e}l\IeC {\'e}scripteurs}{29}{section*.23}% 
\contentsline {paragraph}{\nonumberline Le code ASCII}{30}{section*.25}% 
\contentsline {paragraph}{\nonumberline Unicode}{30}{section*.28}% 
\contentsline {section}{\numberline {1.4}Repr\IeC {\'e}sentation d'images}{32}{section.1.4}% 
\contentsline {section}{\numberline {1.5}Repr\IeC {\'e}sentation des instructions}{34}{section.1.5}% 
\contentsline {section}{\numberline {1.6}D\IeC {\'e}tection et correction d'erreurs}{36}{section.1.6}% 
\contentsline {subsection}{\numberline {1.6.1}Bit de parit\IeC {\'e}}{36}{subsection.1.6.1}% 
\contentsline {subsection}{\numberline {1.6.2}Code de \textsc {Hamming}}{37}{subsection.1.6.2}% 
\contentsline {subsection}{\numberline {1.6.3}Applications des codes correcteurs d'erreur}{40}{subsection.1.6.3}% 
\contentsline {section}{\numberline {1.7}Conclusion: s\IeC {\'e}mantique d'une repr\IeC {\'e}sentation binaire}{41}{section.1.7}% 
