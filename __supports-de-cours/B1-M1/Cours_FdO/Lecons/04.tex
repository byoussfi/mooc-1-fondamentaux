\documentclass[a4paper]{scrartcl}
\usepackage{mathptmx}
\usepackage[scaled=.90]{helvet}
\usepackage{courier}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
%\usepackage{a4wide}
\usepackage[pdftex]{graphicx}
\usepackage{url}
\usepackage{../../foncordi}
%\usepackage{hyperref}
%\usepackage{times}
\usepackage{rotating}
\usepackage{amssymb,amsmath}
\newcommand{\desc}{\ensuremath{\textrm{desc}}}
\newcommand{\pres}{\ensuremath{\textrm{présence}}}

\title{INFO-F-102 -- Fonctionnement des Ordinateurs\\ Leçons 6 à 9 --
  Niveau 0: portes logiques}
\author{Gilles Geeraerts}
\date{Année académique 2016--2017}

\begin{document}
\maketitle

\begin{center}
  \fbox{
    \begin{minipage}{.9\linewidth}
      \begin{center}
        Pensez à réviser les bases de logique Booléenne vue au cours
        de mathématiques !
      \end{center}
    \end{minipage}
  }
\end{center}

\section{Portes logique}
Une porte logique est un dispositif qui calcule un des opérateurs de
la logique Booléenne. Une porte logique est représentée graphiquement
à l'aide d'un symbole distinctif, qui possèdes des entrées et une
sortie, représentées sous forme de traits. On combine les portes
logiques entre elles en connectant la sortie d'une porte aux entrées
d'autres portes, ce qui forme un \emph{circuit}, capable de calculer
une fonction Booléenne. La Fig. 3--2 du livre illustre ces portes.


\subsection{Réalisation des portes logiques}
Concrètement, nous voulons réaliser ces portes logiques à l'aide de
matériel électronique qui possèderont des \emph{connecteurs} (pattes
métalliques ou câbles) d'entrée et de sortie pour obtenir les valeurs
d'entrée et produire les valeurs de sortie. La solution actuellement
utilisée dans les ordinateurs modernes est celle des
transistors. Autrefois, on utilisait des lampes à vide.

\paragraph{Représentation des valeurs logiques} Dans un circuit
électronique, on utilisera des \emph{voltages différents} pour
représenter les valeurs logiques. Typiquement, on utilisera 0 volt
pour \textit{faux} et 5 volts pour \textit{vrai}. 

\paragraph{\`A l'aide de transistors} Les transistors sont des
composant électroniques à 3 connexions appelés respectivement
\emph{base}, \emph{émetteur} et \emph{collecteur}. On en trouve sous
différentes formes% (voir Fig.~\ref{fig:transistors})
, selon la fonction qu'ils doivent réaliser, mais on est aujourd'hui
capable de les miniaturiser énormément (les processeurs actuels ont de
l'ordre de plusieurs milliards de transistor par mm${}^2$).
% \begin{figure}
%   \centering
%   \includegraphics[width=\textwidth]{images/Chap2/transistors.jpg}
%   \caption{Exemples de transistors ({\tt
%       images/Chap2/transistors.jpg}).}
%   % http://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Electronic_component_transistors.jpg/786px-Electronic_component_transistors.jpg
%   \label{fig:transistors}
% \end{figure}

On peut facilement utiliser des transistors pour réaliser des portes
NOT, NAND et NOR (voir Fig. 3--1 du livre de référence).

\paragraph{\`A l'aide de tubes à vide} Les premiers ordinateurs
utilisaient des tubes à vide à la place des transistors. Le principe
étaient \textit{grosso modo} le même, mais les tubes avaient le
désavantage de chauffer, de consommer beaucoup de courant, d'être
fragiles et de prendre de la place.%  Voir Fig.~\ref{fig:tubesibm}

% \begin{figure}
%   \centering
%   \includegraphics[width=\textwidth]{images/Chap2/Ibm-tube.jpg}
%   \caption{Exemple de tubes IBM ({\tt images/Chap2/Ibm-tube.jpg}). }
%   \label{fig:tubesibm}
%   %http://upload.wikimedia.org/wikipedia/commons/2/2b/Ibm-tube.jpg
% \end{figure}

\paragraph{Circuits intégrés} En pratique, il est également possible
d'utiliser des circuits intégrés, qui possèdent plusieurs pattes, et
qui contiennent plusieurs portes logiques (voir Fig. 3--10 du livre)

\section{Circuits pour réaliser l'arithmétique binaire}
Dans cette section, nous allons étudier des circuits logiques qui
permettent de réaliser des opérations arithmétiques sur des nombres
représentés en binaire. Nous allons donc fixer un nombre $n$ de bits
pour représenter ces nombres, et réaliser des circuits qui ont
$k\times n$ entrées (s'il y a $k$ entrées à l'opération, par exemple
$k=2$ pour l'addition) et $\ell\times n$ sorties (s'il y a $\ell$
sorties à l'opération, par exemple $\ell=1$ pour l'addition)


\subsection{Demi-additionneur}
Un demi-additionneur réalise la somme de deux bits $a$ et $b$ et
produit deux sorties: la somme $s$ et un report $r$. La table de
vérité est la suivante:
\begin{center}
  \begin{tabular}{|c|c||c|c|}
    \hline
    $a$&$b$&$s$&$r$\\
    \hline\hline
    0&0&0&0\\\hline
    0&1&1&0\\\hline
    1&0&1&0\\\hline
    1&1&0&1\\\hline
  \end{tabular}
\end{center}
On voit donc que $s=(a\wedge \neg b)\vee (\neg a \wedge b)= a\
\mathrm{XOR}\ b$. Par ailleurs, $r=a\wedge b$ (il n'y a report que si
les deux bits d'entrée sont à un).

Pour le circuit voir \url{DemiAdd1bit.circ} ou Fig. 3--17 du livre. 

\subsection{Additionneur}
De manière générale, quand on veut additionner deux nombres sur
plusieurs bits, il faut additionner les bits de chaque nombre deux à
deux \emph{et tenir compte du report} de la colonne précédente. On a
donc besoin d'un additionneur à trois entrées: $a$, $b$, $r_{prec}$ (le
report provenant de la colonne précédente) et on a toujours deux
sorties: $s$, et $r_{suiv}$ (la valeur à reporter pour l'addition
suivante).
\begin{center}
  \begin{tabular}{|c|c|c||c|c|}
    \hline
    $a$&$b$&$r_{prec}$&$s$&$r_{suiv}$\\
    \hline\hline
    0&0&0&0&0\\\hline 
    0&0&1&1&0\\\hline
    0&1&0&1&0\\\hline 
    0&1&1&0&1\\\hline
    1&0&0&1&0\\\hline
    1&0&1&0&1\\\hline
    1&1&0&0&1\\\hline 
    1&1&1&1&1\\\hline
  \end{tabular}
\end{center}
On a donc:
\begin{eqnarray*}
  s&=&(\neg a\wedge \neg b\wedge r_{prec})\vee(\neg a \wedge
  b\wedge \neg r_{prec})\vee (a \wedge \neg b\wedge \neg r_{prec})\vee
  (a\wedge b\wedge r_{prec})\\
  &=&\Big(\big((\neg a\wedge \neg b) \vee (a\wedge b)\big)\wedge r_{prec}\Big)\vee \Big(\big((\neg a\wedge b) \vee (a\wedge \neg b)\big)\wedge \neg r_{prec}\Big)\\
  &=& \Big(\big((\neg a\wedge \neg b) \vee (a\wedge b)\big)\wedge r_{prec}\Big)\vee \Big(a\ \mathrm{XOR}\ b\wedge \neg r_{prec}\Big)\\
  &=&\Big(\neg\big(a\ \mathrm{XOR}\ b\big)\wedge r_{prec}\Big)\vee \Big(a\ \mathrm{XOR}\ b\wedge \neg r_{prec}\Big)\\
  &=&a\ \mathrm{XOR}\ b\ \mathrm{XOR}\ r_{prec}
\end{eqnarray*}
Ce résultat peut s'obtenir plus facilement en constatant qu'une
succession de XOR permet de savoir s'il y a un nombre impair de 1 parmi
les valeurs considérées. Ainsi $x_1\ \mathrm{XOR}\ x_2\cdots
\mathrm{XOR}\ x_n=1$ ssi il y a un nombre impair de valeurs à $1$
parmi $x_1,\ldots, x_n$. Et en effet, $s=1$ ssi le nombre de valeurs à
$1$ parmi $a$, $b$ et $r_{prec}$ est impair.

Pour $r_{suiv}$ on a:
\begin{eqnarray*}
  r_{suiv}&=&(\neg a\wedge b\wedge r_{prec})\vee (a\wedge \neg b\wedge r_{prec})\vee (a\wedge b\wedge \neg r_{prec})\vee (a\wedge b\wedge r_{prec})\\
  &=&(\neg a\wedge b\wedge r_{prec})\vee (a\wedge \neg b\wedge r_{prec})\vee \big((a\wedge b\wedge (r_{prec}\vee \neg r_{prec})\big)\\
  &=&(\neg a\wedge b\wedge r_{prec})\vee (a\wedge \neg b\wedge r_{prec})\vee (a\wedge b)\\
  &=& \Big(r_{prec}\wedge \big((\neg a\wedge b)\vee (a\wedge \neg b)\big)\Big)\vee (a\wedge b)\\
  &=&(r_{prec}\wedge a\ \mathrm{XOR}\ b)\vee (a\wedge b)
\end{eqnarray*}
\`A nouveau, on peut re-trouver ce résultat en analysant le problème
autrement: il y aura un report à la colonne suivante:
\begin{enumerate}
\item Soit si $a$ et $b$ valent $1$ simultanément, et ceci,
  indépendemment de $r_{prec}$.
\item Soit si la somme de $a$ et $b$ (sans report) fait $1$, et que le
  report précédent vaut $1$ aussi; c'est-à-dire si $a\ \mathrm{XOR}\
  b=1$ et $r_{prec}=1$.
\end{enumerate}

Pour le circuit, voir Fig. 3--18 du livre ou fichier Logisim
\url{Add1bit.circ}.

En combinant plusieurs additionneurs sur 1 bit, on peut additionner
des nombres sur plusieurs bits, voir Fig. 3--20 pour le principe, ou
fichier Logisim \url{Add3bits.circ}.


\subsection{Décalage}
Il s'agit d'un circuit qui va réaliser un décalage d'un bit soit vers
la droite soit vers la gauche, pour obtenir respectivement une
division par $2$ ou une multiplication par $2$. Pour l'exemple, nous
nous contenterons de nombres de $3$ bits. Cet opérateur aura donc 4
entrées:
\begin{itemize}
\item Les entrées $D_0$, $D_1$, $D_2$ qui sont les trois bits du
  nombre à décaler. $D_0$ est le bit le plus à gauche ;
\item Une entrée $C$ qui indique dans quel sens effectuer le
  décalage: vers la droite si $C$ est vrai, vers la gauche si $C$ est
  faux ;
\end{itemize}
et 3 sorties, à savoir les $3$ bits $S_0$, $S_1$, $S_2$ du nombre en
sortie. Si le décalage a lieu vers la droite, on mettra $0$ dans
$S_0$, et si le décalage a lieu vers la gauche, on mettre $0$ dans
$S_2$.

On a donc les tables de vérité suivantes:
\begin{center}
  \begin{tabular}{|c|c||c|}
    \hline
    $C$&$D_1$&$S_0$\\
    \hline\hline
    0&0& 0\\\hline
    0&1& 1\\\hline
    1&0& 0\\\hline
    1&1& 0\\\hline
  \end{tabular}
\end{center}
On voit donc que $S_0=\neg C \wedge D_1$.

\begin{center}
  \begin{tabular}{|c|c|c||c|}
    \hline
    $C$&$D_0$&$D_2$&$S_1$\\
    \hline\hline
    0&0&0&0 \\\hline
    0&0&1&1 \\\hline
    0&1&0&0 \\\hline
    0&1&1&1 \\\hline
    1&0&0&0 \\\hline
    1&0&1&0 \\\hline
    1&1&0&1 \\\hline
    1&1&1&1 \\\hline
  \end{tabular}
\end{center}
On voit donc que
\begin{eqnarray*}
  S_1&=&(\neg C\wedge \neg D_0\wedge D_2)\vee (\neg C
  \wedge D_0\wedge D_2)\vee (C\wedge D_0\wedge \neg D_2)\vee (C\wedge
  D_0\wedge D_2)\\
  &=& \big(\neg C\wedge D_2 \wedge (\neg D_0\vee D_0)\big)\vee \big(C \wedge D_0 \wedge (D_2\vee \neg D_2)\big)\\
  &=&(\neg C\wedge D_2)\vee (C\wedge D_0)
\end{eqnarray*}
On voit bien que $C$ sert à sélectionner la valeur d'entrée qui
détermine la valeur de $S_1$: soit $D_0$ soit $D_2$.


\begin{center}
  \begin{tabular}{|c|c||c|}
    \hline
    $C$&$D_1$&$S_2$\\
    \hline\hline
    0&0& 0\\\hline
    0&1& 0\\\hline
    1&0& 0\\\hline
    1&1& 1\\\hline
  \end{tabular}
\end{center}
On voit donc que que $S_2= C\wedge D_1$.

Pour le circuit voir \url{Decallage3bits.circ} ou la Fig. 3--16 du
livre (sur 8 bits).



\subsection{ALU simplifiée (1 bit)}
Une ALU doit avoir essentiellement 3 entrées:
\begin{itemize}
\item 2 valeurs $A$ et $B$ sur lesquelles il faut réaliser des
  opérations.
\item 1 valeur $I$ indiquant l'opération à effectuer.
\end{itemize}
Dans certains cas, des entrées et sorties additionnelles peuvent être
nécessaires. Par exemple, si l'on demande d'effectuer une addition, on
peut désirer spécifier un report additionnel, et obtenir un report en
sortie. Remarquons également que l'entrée $I$ peut être représentée
sur plusieurs bits. Cela dépend naturellement du nombre d'opérations à
effectuer.

Nous considérons 2 exemples. Le premier est une ALU à 1 bit, admettant
deux opérations: $A\wedge B$ et $A\vee B$. On a donc:
\begin{itemize}
\item Une entrée sur 1 bit pour $A$.
\item Une entrée sur 1 bit pour $B$.
\item Une entrée sur 1 bit pour l'opération $I$ : si $I=1$, on
  calculera un <<~ou~>>, sinon, un <<~et~>>.
\item Une sortie sur 1 bit: le résultat de l'opération.
\end{itemize}
Voir le fichier \url{ALU1bit2operations.circ}. L'ALU comprend 3
étages: au premier étage on calcule les deux opérations, au deuxième
étage, on sélectionne l'opération à effectuer à l'aide d'un ET et de
l'entrée~$I$. Grâce à cette sélection on a la garantie que le ET qui
correspond à l'opération qui n'est pas choisie aura sa sortie à
faux. Le 3\ieme{} étage est constitué d'un OU dont une seule ligne
d'entrée au plus sera activée.


L'exemple \url{ALU1bit4operations.circ} présente une ALU plus
complexe. Elle est capable de calculer 4 opérations différentes:
$A\wedge B$, $A\vee B$, $\neg B$, et la somme de $A$ et $B$ et d'un
report $r$. On a donc:
\begin{itemize}
\item 3 entrées pour $A$, $B$ et le report $r$.
\item 2 entrées $F_0$ et $F_1$ pour sélectionner l'opération:
  \begin{center}
    \begin{tabular}{|c|c||c|}
      \hline
      $F_0$&$F_1$&opération\\\hline\hline
      0&0& et\\\hline
      0&1& ou \\\hline
      1&0& négation de $B$\\\hline
      1&1& somme de $A$, $B$ et du report\\\hline
    \end{tabular}
  \end{center}
\end{itemize}
l'ALU comprend 4 parties:
\begin{enumerate}
\item Une partie qui calcule chacune des opérations
\item Une partie qui <<~décode~>> l'opération à effectuer. Cette
  partie possède deux entrées $F_0$ et $F_1$ et quatre sorties qui
  sont \emph{mutuellement exclusive}, c'est-à-dire qu'une seule
  d'entre elles peut être égale à 1 à tout moment. La sortie
  <<~\textsf{Select ET}~>> sera à 1 ssi l'entrée sur $F_0$ et $F_1$
  indique qu'un <<~et~>> doit être calculé, c'est-à-dire si
  $F_0=F_1=0$, \textit{etc}.
\item Un <<~étage~>> de portes ET qui sélectionne la bonne valeur pour
  la sortie en fonction de la ligne qui a été activée par le décodeur.
\item Un OU pour connecter les 4 lignes à la sortie.
\end{enumerate}

\section{Circuits pour réaliser des mémoires}

\subsection{Horloges} 
Afin de fixer un ordre sur les opérations qui ont lieu dans les
circuits, et de préciser les moments auxquels ces événements ont lieu,
on a souvent recours à des \emph{horloges}, qui sont des petits
circuits émettant des pulsations à intervalles réguliers. Par
<<~pulsation~>>, on entend un changement de l'état bas à l'état haut,
suivi un peu plus tard par un changement de l'état vers l'état bas sur
une ligne donnée (voir Fig. 3--21 du livre). On verra dans la suite
comment on peut utiliser ces horloges:


\subsection{Bascules} 
\paragraph{Principe de base} Une bascule (\textit{latch} en anglais)
est un circuit qui possède deux entrées: $S$ et $R$ pour \textit{set}
et \textit{reset} et deux sorties $Q$ et $\overline{Q}$ qui sont
toujours l'inverse l'une de l'autre. Pour réaliser une mémoire, on
s'intéresse à la sortie $Q$, par exemple, qui va retenir la valeur
d'un bit. En consultant la sortie $Q$, on lit donc la mémoire. Pour
écrire dans la mémoire, on met soit l'entrée $S$ à $1$ (et l'entrée
$R$ à $0$) pour forcer $Q$ à $1$. En mettant $S=0$ et $R=1$, on force
$Q$ à $0$. Par contre, \textbf{dès que les deux entrées repassent à
  $0$, les valeurs de $Q$ et $\overline{Q}$ ne changent pas tant que
  ces valeurs d'entrées sont maintenues, et la bascule se comporte
  donc comme une mémoire}.

On voit que $Q$ doit être vrai, à un moment donné, \emph{si et
  seulement si}:
\begin{itemize}
\item $Q$ était vrai précédemment et
\item $R$ n'est pas vrai
\end{itemize}
Autrement dit, comme $Q$ et $\overline{Q}$ sont toujours l'inverse
l'un de l'autre, on voir que $Q$ doit être vrai \emph{si et seulement
  si}:
\begin{itemize}
\item $\overline{Q}$ est faux et
\item $R$ est faux
\end{itemize}

Autrement dit, on a: $Q=\overline{Q}\ \mathrm{NOR}\
R$. Symétriquement, on a: $\overline{Q}= Q\ \mathrm{NOR}\ S$.

La particularité de ce circuit est donc que les entrées dépendent des
sorties. Par ailleurs, on observe qu'on ne peut pas avoir $S=R=1$, car
alors le circuit serait dans un état incohérent. On devrait avoir:
\begin{itemize}
\item $Q=\overline{Q}\ \mathrm{NOR}\ 1=0$ et
\item $\overline{Q}=Q\ \mathrm{NOR}\ 1=0$ et
\item $Q=\neg\overline{Q}$.
\end{itemize}
ce qui est impossible. Néanmoins, cette restriction n'est pas
problématique étant donné que cela correspondrait à demander à la
mémoire d'écrire $0$ et $1$ en même temps\ldots

On a donc les entrées suivantes possible pour ce circuit:
\begin{enumerate}
\item $S=1$ et $R=0$: écrit un <<~1~>> dans la mémoire: $Q=1$ et
  $\overline{Q}=0$.
\item $S=0$ et $R=1$: écrit un <<~0~>> dans la mémoire: $Q=0$ et
  $\overline{Q}=1$.
\item $S=R=0$: maintient l'état précédent. La sortie ne dépend donc
  pas uniquement de l'entrée.
\item $S=R=1$: interdit.
\end{enumerate}

Voir fichier Logisim \url{Bistable.circ} ou Fig. 3--22 du livre.

\paragraph{Bascule avec horloge} Comme nous l'avons déjà indiqué, une
horloge peut être utilisée pour indiquer à quel moment les circuits
logiques doivent changer d'état. Dans le cas des bascules, on peut
utiliser l'horloge pour faire en sorte que la bascule n'enregistre le
changement d'état qu'aux seuls moments où l'horloge est à $1$. Ainsi,
on peut avoir le schéma de fonctionnement suivant:
\begin{itemize}
\item Pendant que l'horloge est à l'état bas, les entrées de la
  bascule changent (en fonction des calculs qui sont effectués par un
  circuit en amont).
\item Les entrées de la bascule finissent par se stabiliser.
\item L'horloge passe à l'état haut, la bascule enregistre
  l'information.
\item L'horloge repasse à l'état bas: l'information est <<~gelée~>>
  durant toute cette période.
\end{itemize}

Ce phénomène est obtenu simplement en connectant les deux entrées,
\emph{via} deux portes ET, à l'horloge. Ainsi, quand l'horloge est à
l'état bas, les entrées sont annihilées pour la bascule.

Voir fichier Logisim \url{BistableAvecHorloge.circ} ou Fig. 3--23 du
livre.

\paragraph{Bistable $D$} Pour éviter les entrées du type $S=R=1$, on
ne garde qu'une seule entrée $D$ à la bascule qui est connectée
directement à $S$, ainsi qu'à $R$ \emph{via} un NOT. Ainsi, si $D=1$,
on a $S=1$ et $R=0$. Si $D=0$, on a $S=0$ et $R=1$. Cette façon de
faire interdit $S=R=1$ mais aussi $S=R=0$, et on a donc $D=Q$. Cette
façon de faire n'a d'intérêt qu'en combinaison avec l'horloge, car
alors nous avons trois cas:
\begin{itemize}
\item Soit l'horloge est à l'état bas, et donc $S=R=0$, quelque soit
  la valeur d e $D$. La mémoire ne change donc pas d'état malgré les
  variations possibles de $D$ (elle <<~mémorise~>>).
\item Soit l'horloge est à l'état haut, et:
  \begin{itemize}
  \item Si $D=0$, on a $S=0$ et $R=1$: on écrit un 0.
  \item Si $D=1$, on a $S=1$ et $R=0$: on écrit un 1.
  \end{itemize}
\end{itemize}
De cette façon, on écrit dans la mémoire uniquement quand l'horloge
est à l'état haut, et on évite l'état $S=R=1$.

Voir fichier Logisim \url{Bistable-D-AvecHorloge.circ} ou Fig. 3--24
du livre.

\subsection{Flip-flops}
Un problème avec les bascules est que l'état de la bascule peut varier
librement durant tout le temps où l'horloge est à 1. Cela force donc à
créer, en amont, un circuit qui soit capable de garder une sortie
stable durant ce laps de temps. Afin d'éviter cette difficulté, on
utilise souvent des \emph{flip-flops}, qui sont des bascules de type
$D$ à horloge, mais dont l'état change en fonction de $D$, \emph{au
  moment précis où l'horloge change d'état}. Ainsi, tant que l'horloge
reste à une valeur stable (que ce soit $0$ ou $1$), la mémoire
maintient son état, quelque soit les changements de $D$. Quand
l'horloge change d'état, l'ordre d'écriture indiqué sur $D$ est pris
en compte.

Pour réaliser un flip-flop, on peut utiliser une bascule et
intercaler, entre l'horloge et la bascule, un \textit{générateur de
  pulsation}, qui est dispositif qui va générer une pulsation très
courte au moment où l'horloge change d'état. La sortie de ce
générateur va donc se comporter comme une horloge qui passe très
rapidement de 0 à 1 et de 1 à 0 lorsque son entrée change.

Concrètement, cela peut être réalisé comme indiqué à la Fig. 3--25 du
livre, en exploitant le fait qu'il y a un délai très court de
propagation du courant dans la porte NOT\footnote{Remarquons que ce
  phénomène ne peut pas être simulé dans Logisim, mais qu'il existe
  des flip-flop <<~tout faits~>> dans Logisim. Remarquons enfin que
  dans Logisim, les \emph{flip-flops} sont soit sur \emph{flanc
    montant} soit sur \emph{flanc descendant}: c'est le passage de $0$
  à $1$ ou de $1$ à $0$ respectivement qui active la mémorisation.}.

Voir Fichier Logisim \url{FlipFlop.circ}.

\subsection{Exemple de mémoire et types de mémoires}
Utilisation des bascules et flip-flops pour faire une mémoire RAM/ROM:
voir \url{Memoire2mots3bits.circ}. Cette mémoire peut stocker 2 mots
de 3 bits. Elle possède 6 entrées:
\begin{itemize}
\item Les entrées $I_0$, $I_1$ et $I_2$ pour les données (en écriture)
\item L'entrée $A$ pour sélectionner le mot (=adresse du mot en
  mémoire, comme il n'y a que deux mots, un bit suffit).
\item L'entrée $CS$ qui active la mémoire: dans un ordinateur, il
  existe \emph{plusieurs} puces de mémoire, qu'il faut activer en
  fonction de la donnée qu'on souhaite lire.
\item L'entrée $RD$ qui met la mémoire en mode lecture. Quand cette
  ligne passe de $1$ à $0$ (si les \emph{flip-flops} sont sur flanc
  montant), les données présente sur $I_0$, $I_1$ et $I_2$ sont
  écrites à l'adresse précisée par $A$.
\end{itemize}
Elle possède trois sorties: $O_0$, $O_1$ et $O_2$ qui permettent, à
tout moment (peut importe si $RD$ est à $0$ ou à $1$) de lire le
contenu du mot à l'adresse indiquée par $A$.

Observer qu'il existe plusieurs \emph{lignes} qui sortent de la partie
qui <<~décode~>> l'adresse mémoire pour activer les bon
\textit{flip-flops} en écriture (\textit{via} les entrées horloge des
\textit{flip-flops}) et pour sélectionner la bonne information en
lecture.

% \begin{sidewaysfigure}
%   \centering
%   \includegraphics[height=15cm]{../images/Chap2/memoire2mots3bits.png}
%   \caption{Mémoire 2 mots 3 bits ({\tt
%       images/Chap2/memoire2mots3bits.png}). }
%   \label{fig:mem}
% \end{sidewaysfigure}

\paragraph{Autre type d'organisation des mémoires} La mémoire accepte
un numéro de colonne et un numéro de ligne puis répond par le bit en
question (voir Fig. 3--31). Les mémoires modernes répondent plusieurs
bits en une fois (voir Fig. 3--32).
\end{document}
