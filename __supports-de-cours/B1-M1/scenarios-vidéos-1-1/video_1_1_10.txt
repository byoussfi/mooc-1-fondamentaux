MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_10
durée : 2'

Titre : Représentation d'images, de sons, de vidéos


Dans cette vidéo, nous allons nous intéresser à la représentation des contenus multimédia: les images, le son et les vidéos.

Le principe général sera toujours le même qu'avec les caractères: représenter les éléments de l'image, du son ou de la vidéo à l'aide de nombres binaires.


Commençons par les images bitmap. 

Cette famille d'images est celle qui est utilisée quand on fait des photos à l'aide de son téléphone mobile ou d'un appareil photo numérique.

Un image bitmap est constituée d'une grille d'éléments qu'on appelle des pixels. Il s'agit de l'abréviation de l'anglais picture element, c'est-à-dire élément d'image.

Chaque pixel est un point de l'image et il possède une couleur unique.

Naturellement, plus il y a de pixels dans l'image, et plus l'image est précise. 

Le nombre de pixels (ou de Megapixels) affichés par un écran de télévision ou capturé par un appareil photo numérique est donc un argument de vente important.


Pour exprimer notre image sous forme d'une suite de nombres binaires, il faut donc associer un nombre à la couleur de chaque pixel.

Pour ce faire, on peut utiliser l'encodage RVB (pour rouge, vert, bleu, ou RGB en anglais). 

Cela consiste à décomposer chaque couleur en une combinaison de rouge, de vert et de bleu. Ensuite, on exprime en binaire l'intensité de chacune de ces trois couleurs dans le mélange

Par exemple, le turquoise en haut des slides est composé:

- de rouge avec une intensité de 30,6 %

- de vert avec une intensité de 64,7 %

- et de bleu avec une intensité de 61,6 %

On peut exprimer chacun de ces trois pourcentages sur 8 bits, où 100% correspond à la valeur maximum, soit tous les bits à 1. 

Cela nous donne un nombre sur 3 octets qui caractérise la couleur. 

En hexadécimal, dans notre exemple, ce serait 4EA59D


Ensuite, pour représenter toute l'image, il suffit d'énumérer la suite des valeurs qui correspond à tous ses pixels.

Notons enfin qu'un fichier image peut contenir d'autres informations, comme le modèle d'appareil photo utilisé, ou la date de prise de vue. 

Ces informations sont en général appelées les données EXIF.



Intéressons-nous maintenant à la question du son. Un son est une onde sonore comme on en voit un exemple sur le transparent.

Pour en obtenir une représentation informatique, il faut passer par deux étapes.

La première est l'échantillonnage: cela revient à mesurer de manière régulière l'intensité du signal sonore. 

Chaque mesure est appelée un échantillon et la fréquence à laquelle cet échantillonnage a lieu est mesurée en Hertz.

Par exemple, sur les disques compacts, on utilise une fréquence de 44,1 kHz, ce qui signifie qu'on mesure un échantillon du signal sonore 44 100 fois par seconde.


La seconde étape est la quantification, qui consiste à transformer chaque échantillon en un nombre entier. 

Pour ce faire, on fixe une résolution, qui indique sur combien de bits sera représenté chaque échantillon. 

Par exemple, pour les disques compacts à nouveau, on a des échantillons sur 16 bits. Cela signifie qu'on peut mesurer 2^16,  soit 65536 valeurs différentes.


Naturellement, une plus grande fréquence d'échantillonnage ou une plus grande résolution permettent d'améliorer la qualité de l'enregistrement.



Enfin, les formats vidéos sont obtenus en règle général en combinant des représentations pour l'image et pour le son, comme on peut s'y attendre.


Notons pour terminer que la quantité de données nécessaire pour stocker un son, une image ou une vidéo en haute résolution peut vite devenir très importante. 

Cela pose de nombreuses difficultés en matière de stockage ou de transmission de ces contenus (pensez aux contenus de streaming, par exemple).

C'est pourquoi les formats utilisés en pratique mettent en œuvre des techniques de compression, qui permettent de réduire fortement la taille des fichiers, tout en conservant une qualité suffisante.

En effet, la compression peut s'effectuer sans avec avec perte selon que l'on puisse toujours retrouver ou pas les données d'origine.

Par exemple, pour les images, le format JPEG utilise de la compression. 

Pour les fichiers sons, on trouve les formats MP3 ou FLAC. 

Enfin, pour la vidéo, on trouve par exemple le format MPEG.


Tous ces formats précisent quelles sont les techniques de compression à utiliser. Ces techniques sont implémentées dans des programmes qu'on appelle des CODEC, qui est l'abréviation de "Codeur - Décodeur".


